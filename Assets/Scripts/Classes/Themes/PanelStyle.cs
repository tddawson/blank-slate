using UnityEngine;

[CreateAssetMenu(fileName = "PanelStyle", menuName="Themes/Panel Style")]
public class PanelStyle : ScriptableObject {
    public Color color;
    public Sprite sprite;
}