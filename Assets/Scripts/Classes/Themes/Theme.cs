using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Theme", menuName="Themes/Theme")]
public class Theme : ScriptableObject {

    new public string name;
    public PanelStyle topPanelStyle;
    public PanelStyle leftPanelStyle;
    public PanelStyle bottomPanelStyle;
    public PanelStyle rightPanelStyle;
    private bool unlocked;
    private string unlockInstructions;
    private List<int> scores = new List<int>();

    public bool Unlocked {
        get { return unlocked; }
        set { unlocked = value; }
    }
    public string UnlockInstructions { get { return unlockInstructions; }}

    public PanelStyle PanelStyleByDirection(int dir) {
        switch (dir) {
            case 0: return topPanelStyle;
            case 1: return leftPanelStyle;
            case 2: return bottomPanelStyle;
            case 3: return rightPanelStyle;
            default: throw new System.Exception("Direction must be 0, 1, 2, or 3.");
        }
    }

    public void AddScore(int score) {
        scores.Add(score);
    }

    public int HighScore() {
        int max = 0;
        foreach (int score in scores) {
            max = Mathf.Max(max, score);
        }
        return max;
    }

    public int AvgScore() {
        if (scores.Count == 0) return 0;

        int total = 0;
        foreach (int score in scores) {
            total += score;
        }

        return Mathf.RoundToInt(1.0f * total / scores.Count);
    }
}