using UnityEngine;


[CreateAssetMenu(fileName = "ThemeManager", menuName="Themes/Theme Manager")]
public class ThemeManager : ScriptableObject {

    public Theme[] themes;
    public int selectedThemeIndex;

    public Theme SelectedTheme() {
        return themes[selectedThemeIndex];
    }
}