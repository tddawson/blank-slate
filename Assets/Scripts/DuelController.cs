﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DuelController : MonoBehaviour
{

    public GameObject gameOverContainer;
    public GameObject divider;
    public AudioSource bgMusic;
    public AudioSource failSound;
    public AudioSource[] safeSounds;

    public GameObject panelContainer;
    public Sprite panelSprite;
    public PanelController currentPanel;

    private int dir = 0;
    private bool playing = true;
    private float[] bounds = { 0, 0, 0, 0 };
    private float halfwayPoint;

    public ThemeManager themeManager;
    public Theme theme;


    // Use this for initialization
    void Start()
    {
        theme = themeManager.SelectedTheme();

        halfwayPoint = Screen.height / 2;
        bounds[0] = halfwayPoint - 5; // Subtract half distance of divider
        bounds[2] = halfwayPoint - 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (playing && Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.y > halfwayPoint && currentPanel && currentPanel.dir != 0)
            {
                // Top player clicked out of turn
                currentPanel.StopMoving();
                GameOver(1);
            }
            else if (Input.mousePosition.y < halfwayPoint && currentPanel && currentPanel.dir != 2)
            {
                // Bottom player clicked out of turn
				currentPanel.StopMoving();
                GameOver(0);
            }
            else {
				if (currentPanel)
				{
					// Don't play sound for starting touch
					PlaySafeSound();
				}
				CreatePanel();
			}
        }
    }

    private void CreatePanel()
    {
        if (currentPanel)
        {
            float bound = currentPanel.StopMoving();
            if (currentPanel.dir == 0)
            {
                bounds[0] = bound;
                bounds[2] = Mathf.Min(bounds[2], Screen.height - bound);
            }
            else if (currentPanel.dir == 2)
            {
                bounds[2] = bound;
                bounds[0] = Mathf.Min(bounds[0], Screen.height - bound);

                // Hide divider now that both have their self-imposed limits.
                divider.SetActive(false);
            }
        }
        GameObject panel = new GameObject("Panel");
        panel.AddComponent<CanvasRenderer>();
        Image i = panel.AddComponent<Image>();
        PanelStyle panelStyle = theme.PanelStyleByDirection(dir);
        i.sprite = panelStyle.sprite ? panelStyle.sprite : panelSprite;
		i.color = panelStyle.color;
		i.type = Image.Type.Tiled;

        SetPanelAnchor(panel, dir);

        panel.transform.SetParent(panelContainer.transform, false);
        PanelController panelController = panel.AddComponent(typeof(PanelController)) as PanelController;
        panelController.leadingEdgeSprite = panelSprite;
        panelController.dir = dir;
        panelController.deathPoint = bounds[dir];
        panelController.duel = this;
        currentPanel = panelController;

        dir += 2; // Skip sides.
        dir %= 4;
    }

    private void PlaySafeSound()
    {
        int index = Random.Range(0, safeSounds.Length);
        safeSounds[index].Play();
    }

    private void SetPanelAnchor(GameObject panel, int dir)
    {
        RectTransform rectTransform = panel.GetComponent<RectTransform>();
        if (dir == 0)
        {
            // TOP
            rectTransform.pivot = new Vector2(.5f, 1f);
            rectTransform.anchorMin = new Vector2(0, 1f);
            rectTransform.anchorMax = new Vector2(1f, 1f);
        }
        else if (dir == 2)
        {
            // BOTTOM
            rectTransform.pivot = new Vector2(.5f, 0f);
            rectTransform.anchorMin = new Vector2(0, 0f);
            rectTransform.anchorMax = new Vector2(1f, 0f);
        }
        rectTransform.sizeDelta = new Vector2(0f, 0f);
    }

    public void GameOver(int winner = -1)
    {
		if (winner == -1) {
			if (currentPanel.dir == 0) {
				winner = 1;
			} else {
				winner = 0;
			}
		}
        playing = false;
        bgMusic.Stop();
        failSound.Play();
        gameOverContainer.SetActive(true);
		if (winner == 0) {
			gameOverContainer.transform.Rotate(new Vector3(0f, 0f, 180f));
		}
    }

    public void StartOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
