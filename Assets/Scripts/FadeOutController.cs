﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOutController : MonoBehaviour {

	private bool fading = false;
	private float lerpTimer;
	public float fadeSpeed;
	private Image i;

	void Start() {
		i = GetComponent<Image>();
	}
	public void StartFading() {
		fading = true;
	}

	void Update() {
		if (fading) {
			float diff = Time.deltaTime * fadeSpeed;
			float a = i.color.a;
			a = Mathf.Max(a - diff, 0f);
			i.color = new Vector4(1.0f, 1.0f, 1.0f, a);
			if (a == 0f) {
				Destroy(gameObject);
			}
		}
	}
}
