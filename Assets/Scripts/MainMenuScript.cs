﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

	public GameObject colorSelectionContainer;
	public GameObject colorSelectionPrefab;
	public float padding = 40f;
	public int themeIndex;

	public void Start() {
		themeIndex = PlayerPrefs.GetInt("theme_index", 0);
		SetTheme(themeIndex);

		// int numThemesUnlocked = PlayerPrefs.GetInt("num_themes_unlocked", 1);
		// if (numThemesUnlocked > 1) {
		// 	SetUpColorSelectionMenu(numThemesUnlocked);
		// 	colorSelectionContainer.SetActive(true);
		// 	Debug.Log("show menu");
		// } else {
		// 	colorSelectionContainer.SetActive(false);
		// 	Debug.Log("hide menu");
		// }
	}

	public void SetTheme(int index) {
		PlayerPrefs.SetInt("theme_index", index);

		// Color topMenuPanelColor = ColorSchemeManager.colorSchemes[index, 0];
		// GameObject[] topMenuPanels = GameObject.FindGameObjectsWithTag("menu_top");
		// for (int i = 0; i < topMenuPanels.Length; i++) {
		// 	topMenuPanels[i].GetComponent<Image>().color = topMenuPanelColor;
		// }

		// Color bottomMenuPanelColor = ColorSchemeManager.colorSchemes[index, 2];
		// GameObject[] bottomMenuPanels = GameObject.FindGameObjectsWithTag("menu_bottom");
		// for (int i = 0; i < bottomMenuPanels.Length; i++) {
		// 	bottomMenuPanels[i].GetComponent<Image>().color = bottomMenuPanelColor;
		// }
	}

	// public void ColorChoiceClicked(GameObject go) {
	// 	// SetTheme(index);
	// }

	// private void SetUpColorSelectionMenu(int numThemes) {
	// 	Debug.Log(numThemes);
	// 	for (int i = 0; i < numThemes; i++) {
	// 		GameObject go = Instantiate(colorSelectionPrefab, new Vector3 (0,0,0), Quaternion.identity) as GameObject; 
	// 		go.transform.SetParent(colorSelectionContainer.transform, false);
	// 		float x = padding + i * (120 + padding);
	// 		Debug.Log(x);
	// 		go.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, 0f);
	// 		go.transform.Find("Panel_top").GetComponent<Image>().color = ColorSchemeManager.colorSchemes[i, 0];
	// 		go.transform.Find("Panel_left").GetComponent<Image>().color = ColorSchemeManager.colorSchemes[i, 1];
	// 		go.transform.Find("Panel_bottom").GetComponent<Image>().color = ColorSchemeManager.colorSchemes[i, 2];
	// 		go.transform.Find("Panel_right").GetComponent<Image>().color = ColorSchemeManager.colorSchemes[i, 3];
	// 		go.GetComponent<MenuColorChoiceController>().SetThemeIndex(i);
	// 	}
	// 	colorSelectionContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(numThemes * (padding + 120) + padding, 200);
	// }

	public void StartGame() {
		SceneManager.LoadScene("Square");
	}

	public void StartDuel() {
		SceneManager.LoadScene("Duel");
	}

	public void LoadThemeSelectionScene() {
		SceneManager.LoadScene("Theme Selection");
	}

	public void LoadPaymentScene() {
		SceneManager.LoadScene("Payment");
	}
}
