﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelContainerController : MonoBehaviour {
	private bool shrinking = false;
	private Vector3 startScale = new Vector3(1.0f, 1.0f, 1.0f);
	public Vector3 endScale = new Vector3(.5f, .5f, .5f);
	private float lerpTimer;
	public float shrinkSpeed;

	private float screenshotCountdown = 0f;
	private bool takingScreenshot = false;

	public GameObject[] hiddenForScreenshot;

	public void StartShrinking() {
		shrinking = true;
	}

	void Update() {
		if (shrinking) {
			transform.localScale = Vector3.Lerp(startScale, endScale, lerpTimer);
			lerpTimer += Time.deltaTime * shrinkSpeed;
		}
		if (takingScreenshot) {
			screenshotCountdown -= Time.deltaTime;
			if (screenshotCountdown < 0f) {
				takingScreenshot = false;
				foreach (GameObject obj in hiddenForScreenshot) {
					obj.SetActive(true);
				}
				shrinking = true;
				lerpTimer = 0;
			}
		}
	}

	public void TakeScreenshot() {
		shrinking = false;
		transform.localScale = startScale;
		foreach (GameObject obj in hiddenForScreenshot) {
			obj.SetActive(false);
		}
		int screenshotId = PlayerPrefs.GetInt("screenshot_id", 0);
		ScreenCapture.CaptureScreenshot("Screenshot-" + screenshotId + ".png");
		PlayerPrefs.SetInt("screenshot_id", screenshotId + 1);
		takingScreenshot = true;
		screenshotCountdown = .5f;
	}
}
