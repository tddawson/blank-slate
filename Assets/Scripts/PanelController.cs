﻿using UnityEngine;
using UnityEngine.UI;

public class PanelController : MonoBehaviour
{

    public bool moving;
    public float speed = .4f;
    public int dir = 0;
    public float deathPoint = 0;
    public SquareController square;
    public DuelController duel;

    

    private RectTransform rectTransform;
    private float adjustedSpeed;

    public Sprite leadingEdgeSprite;
    private GameObject leadingEdge;

    // Use this for initialization
    void Start()
    {
        moving = true;
        rectTransform = GetComponent<RectTransform>();

        adjustedSpeed = speed * Screen.height;

        CreateLeadingEdge();
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            Move();
        }
    }

    private void CreateLeadingEdge() {
        leadingEdge = new GameObject("LeadingEdge");
        leadingEdge.AddComponent<CanvasRenderer>();
        Image i = leadingEdge.AddComponent<Image>();
        i.sprite = leadingEdgeSprite;
        i.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        SetLeadingEdgeAnchor();

        leadingEdge.transform.SetParent(this.transform, false);
    }

    private void SetLeadingEdgeAnchor()
    {
        RectTransform leadingEdgeRectTransform = leadingEdge.GetComponent<RectTransform>();
        if (dir == 0) {
            leadingEdgeRectTransform.pivot = new Vector2(.5f, 0f);
            leadingEdgeRectTransform.anchorMin = new Vector2(0, 0f);
            leadingEdgeRectTransform.anchorMax = new Vector2(1f, 0f);
            leadingEdgeRectTransform.anchoredPosition = new Vector2(0, -5f);
        } else if (dir == 1) {
            leadingEdgeRectTransform.pivot = new Vector2(1f, .5f);
            leadingEdgeRectTransform.anchorMin = new Vector2(1, 0f);
            leadingEdgeRectTransform.anchorMax = new Vector2(1f, 1f);
            leadingEdgeRectTransform.anchoredPosition = new Vector2(5f, 0f);
        } else if (dir == 2) {
            leadingEdgeRectTransform.pivot = new Vector2(.5f, 1f);
            leadingEdgeRectTransform.anchorMin = new Vector2(0, 1f);
            leadingEdgeRectTransform.anchorMax = new Vector2(1f, 1f);
            leadingEdgeRectTransform.anchoredPosition = new Vector2(0, 5f);
        } else if (dir == 3) {
            leadingEdgeRectTransform.pivot = new Vector2(0f, .5f);
            leadingEdgeRectTransform.anchorMin = new Vector2(0, 0f);
            leadingEdgeRectTransform.anchorMax = new Vector2(0f, 1f);
            leadingEdgeRectTransform.anchoredPosition = new Vector2(-5f, 0f);
        }

        // Set height/width
        if (dir % 2 == 0) {
            leadingEdgeRectTransform.sizeDelta = new Vector2(0f, 5f);
        } else {
            leadingEdgeRectTransform.sizeDelta = new Vector2(5f, 0f);
        }
        
    }

    private void Move()
    {
        float diff = adjustedSpeed * Time.deltaTime; // Should also be based off of screen size?
        if (dir == 0 || dir == 2)
        {
            // Up/Down
            Vector2 size = rectTransform.sizeDelta;
            size += new Vector2(0f, diff);
            rectTransform.sizeDelta = size;
            if (size.y > deathPoint)
            {
                GameOver();
            }
        }
        else
        {
            // Left/Right
            Vector2 size = rectTransform.sizeDelta;
            size += new Vector2(diff, 0f);
            rectTransform.sizeDelta = size;
            if (size.x > deathPoint)
            {
                GameOver();
            }
        }
    }

    public float StopMoving()
    {
        Destroy(leadingEdge);
        moving = false;
        if (dir == 0 || dir == 2)
        {
            return rectTransform.sizeDelta.y;
        }
        return rectTransform.sizeDelta.x;
    }

    private void GameOver()
    {
        StopMoving();
        if (square)
            square.GameOver();
        if (duel)
            duel.GameOver();
    }
}
