﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SquareController : MonoBehaviour
{

	private int highScore;
	private bool setHighScore;

	public Text scoreText;
	public GameObject gameOverContainer;
	public Text gameOverScoreText;
	public Color recordColor;
	public Text themeHighText;
	public Text themeAvgText;



	public AudioSource bgMusic;
    public AudioSource drums;
	public AudioSource applause;
    public AudioSource[] accentSounds;

	public AudioSource failSound;
	public AudioSource[] safeSounds;

    public GameObject panelContainer;
	public GameObject allPanelsContainer;
    public Sprite panelSprite;
	public PanelController currentPanel;

	private int dir = 0;
	private int score = 0;
	private bool playing = true;
	private float[] bounds = {0, 0, 0, 0};



	// New Theme Manager stuff
	public ThemeManager themeManager;
	private Theme theme;

	
	public FadeOutController gameOverFade;
	

    void Start()
    {
		theme = themeManager.SelectedTheme();

		bounds[0] = Screen.height;
		bounds[1] = Screen.width;
		bounds[2] = Screen.height;
		bounds[3] = Screen.width;
		highScore = PlayerPrefs.GetInt("high_score", 0);
    }


    void Update()
    {
        if (playing && Input.GetMouseButtonDown(0))
        {
			if (currentPanel)
			{
				// Ignore starting touch
				PlaySafeSound();
				IncreaseScore();
				if (score % 5 == 0) {
					PlayAccentSound();
				}
			}
			CreatePanel();
        }
    }

    private void CreatePanel()
    {
		if (currentPanel)
		{
			float bound = currentPanel.StopMoving();
			if (currentPanel.dir == 0) {
				bounds[0] = bound;
				bounds[2] = Mathf.Min(bounds[2], Screen.height - bound);
			}
			else if (currentPanel.dir == 1) {
				bounds[1] = bound;
				bounds[3] = Mathf.Min(bounds[3], Screen.width - bound);
			}
			else if (currentPanel.dir == 2) {
				bounds[2] = bound;
				bounds[0] = Mathf.Min(bounds[0], Screen.height - bound);
			}
			else if (currentPanel.dir == 3) {
				bounds[3] = bound;
				bounds[1] = Mathf.Min(bounds[1], Screen.width - bound);
			}
		}
        GameObject panel = new GameObject("Panel");
        panel.AddComponent<CanvasRenderer>();
        Image i = panel.AddComponent<Image>();
		PanelStyle panelStyle = theme.PanelStyleByDirection(dir);
        i.sprite = panelStyle.sprite ? panelStyle.sprite : panelSprite;
		i.color = panelStyle.color;
		i.type = Image.Type.Tiled;

        SetPanelAnchor(panel, dir);

        panel.transform.SetParent(allPanelsContainer.transform, false);
		PanelController panelController = panel.AddComponent(typeof(PanelController)) as PanelController;
		panelController.leadingEdgeSprite = panelSprite;
		panelController.dir = dir;
		panelController.deathPoint = bounds[dir];
		panelController.square = this;
		currentPanel = panelController;

		dir++;
		dir %= 4;
    }

	private void PlaySafeSound()
	{
		int index = Random.Range(0, safeSounds.Length);
		safeSounds[index].Play();
	}
    private void PlayAccentSound()
    {
        int index = Random.Range(0, accentSounds.Length);
        accentSounds[index].Play();
    }
		private void IncreaseScore()
	{
		score++;
		scoreText.text = score.ToString();
		if (score > highScore) {
			PlayerPrefs.SetInt("high_score", score);
			if (!setHighScore)
			{
				// This latest panel broke the record
				setHighScore = true;
				Applaud();
			}
		}
	}

	private void Applaud()
	{
		applause.Play();
	}

    private void SetPanelAnchor(GameObject panel, int dir)
    {
        RectTransform rectTransform = panel.GetComponent<RectTransform>();
        if (dir == 0)
        {
            // TOP
            rectTransform.pivot = new Vector2(.5f, 1f);
            rectTransform.anchorMin = new Vector2(0, 1f);
            rectTransform.anchorMax = new Vector2(1f, 1f);
        }
		else if (dir == 1)
        {
            // LEFT
            rectTransform.pivot = new Vector2(0f, .5f);
            rectTransform.anchorMin = new Vector2(0, 0f);
            rectTransform.anchorMax = new Vector2(0f, 1f);
        }
		else if (dir == 2)
        {
            // BOTTOM
            rectTransform.pivot = new Vector2(.5f, 0f);
            rectTransform.anchorMin = new Vector2(0, 0f);
            rectTransform.anchorMax = new Vector2(1f, 0f);
        }
		else if (dir == 3)
		{
			// RIGHT
            rectTransform.pivot = new Vector2(1f, .5f);
            rectTransform.anchorMin = new Vector2(1, 0f);
            rectTransform.anchorMax = new Vector2(1f, 1f);
		}
		rectTransform.sizeDelta = new Vector2(0f, 0f);

    }

	public void GameOver() {
		playing = false;
		bgMusic.Stop();
		failSound.Play();
		scoreText.gameObject.SetActive(false);
		panelContainer.GetComponent<PanelContainerController>().StartShrinking();
		theme.AddScore(score);
		UpdateGameOverGUI();
		gameOverFade.StartFading();
	}

	public void UpdateGameOverGUI() {
		gameOverContainer.SetActive(true);
		gameOverScoreText.text = score.ToString();

		int highScore = theme.HighScore();
		themeHighText.text = highScore.ToString();
		if (score >= highScore) {
			themeHighText.color = recordColor;
		}

		int avgScore = theme.AvgScore();
		themeAvgText.text = avgScore.ToString();
		if (score > avgScore) {
			themeAvgText.color = recordColor;
		}
	}

	public void StartOver() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void ReturnToMenu() {
		SceneManager.LoadScene("Menu");
	}
}
