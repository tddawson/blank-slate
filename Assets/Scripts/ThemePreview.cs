﻿using UnityEngine;
using UnityEngine.UI;

public class ThemePreview : MonoBehaviour
{

    private Theme theme;
    public Image[] topPanels;
    public Image[] leftPanels;
    public Image[] bottomPanels;
    public Image[] rightPanels;
    public Text highScore;
    public Text avgScore;

  
    public Theme Theme {
        get { return theme; }
        set {
            this.theme = value;

            foreach (Image i in topPanels) i.color = theme.topPanelStyle.color;
            foreach (Image i in leftPanels) i.color = theme.leftPanelStyle.color;
            foreach (Image i in bottomPanels) i.color = theme.bottomPanelStyle.color;
            foreach (Image i in rightPanels) i.color = theme.rightPanelStyle.color;

            if (!theme.Unlocked) {
                // transform.Find("Panel Container/locked overlay").gameObject.SetActive(true);
            }

            highScore.text = theme.HighScore().ToString();
            avgScore.text = theme.AvgScore().ToString();
        }
    }
}
