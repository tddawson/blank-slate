﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeSelectionScrollview : MonoBehaviour
{
    public ThemeManager themeManager;
    public GameObject themePreviewPrefab;


    void Start()
    {
        int i = 0;
        foreach (Theme theme in themeManager.themes) {
            GameObject go = Instantiate(themePreviewPrefab, transform);
            go.GetComponent<ThemePreview>().Theme = theme;
            go.GetComponent<RectTransform>().localPosition = new Vector2(800 * i, 0);
            i++;
        }
    }
}
