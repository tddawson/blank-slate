﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// ColorSchemeManager
struct ColorSchemeManager_t1974266384;
// DuelController
struct DuelController_t4245367713;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t631007953;
// PanelController
struct PanelController_t1280165360;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// FadeOutController
struct FadeOutController_t1417150664;
// MainMenuScript
struct MainMenuScript_t2493777029;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// MenuColorChoiceController
struct MenuColorChoiceController_t2734251472;
// MenuPanelController
struct MenuPanelController_t3752352500;
// PanelContainerController
struct PanelContainerController_t2722594607;
// SquareController
struct SquareController_t2553685805;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t941916414;
// System.Void
struct Void_t1185182177;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t4028559421;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;

extern RuntimeClass* ColorU5B0___U2C0___U5D_t941916414_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var;
extern const uint32_t ColorSchemeManager__cctor_m3585789103_MetadataUsageId;
extern RuntimeClass* SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var;
extern const uint32_t DuelController__ctor_m184767228_MetadataUsageId;
extern String_t* _stringLiteral1410563525;
extern String_t* _stringLiteral2718991579;
extern const uint32_t DuelController_Start_m2226500390_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t DuelController_Update_m3698156876_MetadataUsageId;
extern const RuntimeType* PanelController_t1280165360_0_0_0_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* PanelController_t1280165360_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var;
extern String_t* _stringLiteral1597722606;
extern const uint32_t DuelController_CreatePanel_m1900430364_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var;
extern const uint32_t DuelController_SetPanelAnchor_m3823622997_MetadataUsageId;
extern String_t* _stringLiteral1555075383;
extern const uint32_t DuelController_ReturnToMenu_m963487272_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const uint32_t FadeOutController_Start_m3423843087_MetadataUsageId;
extern const uint32_t FadeOutController_Update_m2582507079_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2949325609;
extern String_t* _stringLiteral3717352243;
extern const uint32_t MainMenuScript_Start_m2530389714_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var;
extern String_t* _stringLiteral1297066703;
extern String_t* _stringLiteral2615087991;
extern const uint32_t MainMenuScript_SetTheme_m1616100854_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMenuColorChoiceController_t2734251472_m102361087_RuntimeMethod_var;
extern String_t* _stringLiteral1403620149;
extern String_t* _stringLiteral2199905863;
extern String_t* _stringLiteral3026870195;
extern String_t* _stringLiteral3659458767;
extern const uint32_t MainMenuScript_SetUpColorSelectionMenu_m2362659385_MetadataUsageId;
extern String_t* _stringLiteral3608622578;
extern const uint32_t MainMenuScript_StartGame_m3950745386_MetadataUsageId;
extern String_t* _stringLiteral4287742317;
extern const uint32_t MainMenuScript_StartDuel_m988774669_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisMainMenuScript_t2493777029_m3991380284_RuntimeMethod_var;
extern String_t* _stringLiteral2774581319;
extern const uint32_t MenuColorChoiceController_Clicked_m3268607575_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const uint32_t MenuPanelController_StartMoving_m3728774834_MetadataUsageId;
extern const uint32_t MenuPanelController_Update_m3832367955_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t PanelContainerController_Update_m836685070_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2841973215;
extern String_t* _stringLiteral632663995;
extern String_t* _stringLiteral2410385622;
extern const uint32_t PanelContainerController_TakeScreenshot_m3670517792_MetadataUsageId;
extern const uint32_t PanelController_Start_m564436649_MetadataUsageId;
extern String_t* _stringLiteral1789606354;
extern const uint32_t PanelController_CreateLeadingEdge_m1745492603_MetadataUsageId;
extern const uint32_t PanelController_SetLeadingEdgeAnchor_m2694989816_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t PanelController_Move_m1423420284_MetadataUsageId;
extern const uint32_t PanelController_StopMoving_m198065155_MetadataUsageId;
extern const uint32_t PanelController_GameOver_m2942013208_MetadataUsageId;
extern const uint32_t SquareController__ctor_m2819238152_MetadataUsageId;
extern String_t* _stringLiteral2574543678;
extern const uint32_t SquareController_Start_m688405192_MetadataUsageId;
extern const uint32_t SquareController_Update_m1451841740_MetadataUsageId;
extern const uint32_t SquareController_CreatePanel_m2690008112_MetadataUsageId;
extern const uint32_t SquareController_IncreaseScore_m3643553335_MetadataUsageId;
extern const uint32_t SquareController_UnlockTheme_m880230570_MetadataUsageId;
extern const uint32_t SquareController_SetPanelAnchor_m2372856663_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisPanelContainerController_t2722594607_m3234714783_RuntimeMethod_var;
extern const uint32_t SquareController_GameOver_m1811381354_MetadataUsageId;
extern String_t* _stringLiteral2673082146;
extern String_t* _stringLiteral811457787;
extern String_t* _stringLiteral1390561909;
extern String_t* _stringLiteral2460025446;
extern const uint32_t SquareController_UpdateGameOverGUI_m959589948_MetadataUsageId;
extern const uint32_t SquareController_ReturnToMenu_m1730121986_MetadataUsageId;

struct ColorU5B0___U2C0___U5D_t941916414;
struct SingleU5BU5D_t1444911251;
struct AudioSourceU5BU5D_t4028559421;
struct GameObjectU5BU5D_t3328599146;


#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef COLORSCHEMEMANAGER_T1974266384_H
#define COLORSCHEMEMANAGER_T1974266384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSchemeManager
struct  ColorSchemeManager_t1974266384  : public RuntimeObject
{
public:

public:
};

struct ColorSchemeManager_t1974266384_StaticFields
{
public:
	// UnityEngine.Color[0...,0...] ColorSchemeManager::colorSchemes
	ColorU5B0___U2C0___U5D_t941916414* ___colorSchemes_0;

public:
	inline static int32_t get_offset_of_colorSchemes_0() { return static_cast<int32_t>(offsetof(ColorSchemeManager_t1974266384_StaticFields, ___colorSchemes_0)); }
	inline ColorU5B0___U2C0___U5D_t941916414* get_colorSchemes_0() const { return ___colorSchemes_0; }
	inline ColorU5B0___U2C0___U5D_t941916414** get_address_of_colorSchemes_0() { return &___colorSchemes_0; }
	inline void set_colorSchemes_0(ColorU5B0___U2C0___U5D_t941916414* value)
	{
		___colorSchemes_0 = value;
		Il2CppCodeGenWriteBarrier((&___colorSchemes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSCHEMEMANAGER_T1974266384_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef CANVASRENDERER_T2598313366_H
#define CANVASRENDERER_T2598313366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_t2598313366  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_T2598313366_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public Behaviour_t1437897464
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef PANELCONTROLLER_T1280165360_H
#define PANELCONTROLLER_T1280165360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelController
struct  PanelController_t1280165360  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PanelController::moving
	bool ___moving_2;
	// System.Single PanelController::speed
	float ___speed_3;
	// System.Int32 PanelController::dir
	int32_t ___dir_4;
	// System.Single PanelController::deathPoint
	float ___deathPoint_5;
	// SquareController PanelController::square
	SquareController_t2553685805 * ___square_6;
	// DuelController PanelController::duel
	DuelController_t4245367713 * ___duel_7;
	// UnityEngine.RectTransform PanelController::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// System.Single PanelController::adjustedSpeed
	float ___adjustedSpeed_9;
	// UnityEngine.Sprite PanelController::leadingEdgeSprite
	Sprite_t280657092 * ___leadingEdgeSprite_10;
	// UnityEngine.GameObject PanelController::leadingEdge
	GameObject_t1113636619 * ___leadingEdge_11;

public:
	inline static int32_t get_offset_of_moving_2() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___moving_2)); }
	inline bool get_moving_2() const { return ___moving_2; }
	inline bool* get_address_of_moving_2() { return &___moving_2; }
	inline void set_moving_2(bool value)
	{
		___moving_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_dir_4() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___dir_4)); }
	inline int32_t get_dir_4() const { return ___dir_4; }
	inline int32_t* get_address_of_dir_4() { return &___dir_4; }
	inline void set_dir_4(int32_t value)
	{
		___dir_4 = value;
	}

	inline static int32_t get_offset_of_deathPoint_5() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___deathPoint_5)); }
	inline float get_deathPoint_5() const { return ___deathPoint_5; }
	inline float* get_address_of_deathPoint_5() { return &___deathPoint_5; }
	inline void set_deathPoint_5(float value)
	{
		___deathPoint_5 = value;
	}

	inline static int32_t get_offset_of_square_6() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___square_6)); }
	inline SquareController_t2553685805 * get_square_6() const { return ___square_6; }
	inline SquareController_t2553685805 ** get_address_of_square_6() { return &___square_6; }
	inline void set_square_6(SquareController_t2553685805 * value)
	{
		___square_6 = value;
		Il2CppCodeGenWriteBarrier((&___square_6), value);
	}

	inline static int32_t get_offset_of_duel_7() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___duel_7)); }
	inline DuelController_t4245367713 * get_duel_7() const { return ___duel_7; }
	inline DuelController_t4245367713 ** get_address_of_duel_7() { return &___duel_7; }
	inline void set_duel_7(DuelController_t4245367713 * value)
	{
		___duel_7 = value;
		Il2CppCodeGenWriteBarrier((&___duel_7), value);
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_adjustedSpeed_9() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___adjustedSpeed_9)); }
	inline float get_adjustedSpeed_9() const { return ___adjustedSpeed_9; }
	inline float* get_address_of_adjustedSpeed_9() { return &___adjustedSpeed_9; }
	inline void set_adjustedSpeed_9(float value)
	{
		___adjustedSpeed_9 = value;
	}

	inline static int32_t get_offset_of_leadingEdgeSprite_10() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___leadingEdgeSprite_10)); }
	inline Sprite_t280657092 * get_leadingEdgeSprite_10() const { return ___leadingEdgeSprite_10; }
	inline Sprite_t280657092 ** get_address_of_leadingEdgeSprite_10() { return &___leadingEdgeSprite_10; }
	inline void set_leadingEdgeSprite_10(Sprite_t280657092 * value)
	{
		___leadingEdgeSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___leadingEdgeSprite_10), value);
	}

	inline static int32_t get_offset_of_leadingEdge_11() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___leadingEdge_11)); }
	inline GameObject_t1113636619 * get_leadingEdge_11() const { return ___leadingEdge_11; }
	inline GameObject_t1113636619 ** get_address_of_leadingEdge_11() { return &___leadingEdge_11; }
	inline void set_leadingEdge_11(GameObject_t1113636619 * value)
	{
		___leadingEdge_11 = value;
		Il2CppCodeGenWriteBarrier((&___leadingEdge_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELCONTROLLER_T1280165360_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef MENUCOLORCHOICECONTROLLER_T2734251472_H
#define MENUCOLORCHOICECONTROLLER_T2734251472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuColorChoiceController
struct  MenuColorChoiceController_t2734251472  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MenuColorChoiceController::themeIndex
	int32_t ___themeIndex_2;

public:
	inline static int32_t get_offset_of_themeIndex_2() { return static_cast<int32_t>(offsetof(MenuColorChoiceController_t2734251472, ___themeIndex_2)); }
	inline int32_t get_themeIndex_2() const { return ___themeIndex_2; }
	inline int32_t* get_address_of_themeIndex_2() { return &___themeIndex_2; }
	inline void set_themeIndex_2(int32_t value)
	{
		___themeIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUCOLORCHOICECONTROLLER_T2734251472_H
#ifndef FADEOUTCONTROLLER_T1417150664_H
#define FADEOUTCONTROLLER_T1417150664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeOutController
struct  FadeOutController_t1417150664  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean FadeOutController::fading
	bool ___fading_2;
	// System.Single FadeOutController::lerpTimer
	float ___lerpTimer_3;
	// System.Single FadeOutController::fadeSpeed
	float ___fadeSpeed_4;
	// UnityEngine.UI.Image FadeOutController::i
	Image_t2670269651 * ___i_5;

public:
	inline static int32_t get_offset_of_fading_2() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___fading_2)); }
	inline bool get_fading_2() const { return ___fading_2; }
	inline bool* get_address_of_fading_2() { return &___fading_2; }
	inline void set_fading_2(bool value)
	{
		___fading_2 = value;
	}

	inline static int32_t get_offset_of_lerpTimer_3() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___lerpTimer_3)); }
	inline float get_lerpTimer_3() const { return ___lerpTimer_3; }
	inline float* get_address_of_lerpTimer_3() { return &___lerpTimer_3; }
	inline void set_lerpTimer_3(float value)
	{
		___lerpTimer_3 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_i_5() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___i_5)); }
	inline Image_t2670269651 * get_i_5() const { return ___i_5; }
	inline Image_t2670269651 ** get_address_of_i_5() { return &___i_5; }
	inline void set_i_5(Image_t2670269651 * value)
	{
		___i_5 = value;
		Il2CppCodeGenWriteBarrier((&___i_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEOUTCONTROLLER_T1417150664_H
#ifndef MAINMENUSCRIPT_T2493777029_H
#define MAINMENUSCRIPT_T2493777029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuScript
struct  MainMenuScript_t2493777029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MainMenuScript::colorSelectionContainer
	GameObject_t1113636619 * ___colorSelectionContainer_2;
	// UnityEngine.GameObject MainMenuScript::colorSelectionPrefab
	GameObject_t1113636619 * ___colorSelectionPrefab_3;
	// System.Single MainMenuScript::padding
	float ___padding_4;
	// System.Int32 MainMenuScript::themeIndex
	int32_t ___themeIndex_5;

public:
	inline static int32_t get_offset_of_colorSelectionContainer_2() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___colorSelectionContainer_2)); }
	inline GameObject_t1113636619 * get_colorSelectionContainer_2() const { return ___colorSelectionContainer_2; }
	inline GameObject_t1113636619 ** get_address_of_colorSelectionContainer_2() { return &___colorSelectionContainer_2; }
	inline void set_colorSelectionContainer_2(GameObject_t1113636619 * value)
	{
		___colorSelectionContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___colorSelectionContainer_2), value);
	}

	inline static int32_t get_offset_of_colorSelectionPrefab_3() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___colorSelectionPrefab_3)); }
	inline GameObject_t1113636619 * get_colorSelectionPrefab_3() const { return ___colorSelectionPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_colorSelectionPrefab_3() { return &___colorSelectionPrefab_3; }
	inline void set_colorSelectionPrefab_3(GameObject_t1113636619 * value)
	{
		___colorSelectionPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorSelectionPrefab_3), value);
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___padding_4)); }
	inline float get_padding_4() const { return ___padding_4; }
	inline float* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(float value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_themeIndex_5() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___themeIndex_5)); }
	inline int32_t get_themeIndex_5() const { return ___themeIndex_5; }
	inline int32_t* get_address_of_themeIndex_5() { return &___themeIndex_5; }
	inline void set_themeIndex_5(int32_t value)
	{
		___themeIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUSCRIPT_T2493777029_H
#ifndef DUELCONTROLLER_T4245367713_H
#define DUELCONTROLLER_T4245367713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DuelController
struct  DuelController_t4245367713  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DuelController::gameOverContainer
	GameObject_t1113636619 * ___gameOverContainer_2;
	// UnityEngine.GameObject DuelController::divider
	GameObject_t1113636619 * ___divider_3;
	// UnityEngine.AudioSource DuelController::bgMusic
	AudioSource_t3935305588 * ___bgMusic_4;
	// UnityEngine.AudioSource DuelController::failSound
	AudioSource_t3935305588 * ___failSound_5;
	// UnityEngine.AudioSource[] DuelController::safeSounds
	AudioSourceU5BU5D_t4028559421* ___safeSounds_6;
	// UnityEngine.GameObject DuelController::panelContainer
	GameObject_t1113636619 * ___panelContainer_7;
	// UnityEngine.Sprite DuelController::panelSprite
	Sprite_t280657092 * ___panelSprite_8;
	// PanelController DuelController::currentPanel
	PanelController_t1280165360 * ___currentPanel_9;
	// System.Int32 DuelController::colorScheme
	int32_t ___colorScheme_10;
	// System.Int32 DuelController::dir
	int32_t ___dir_11;
	// System.Boolean DuelController::playing
	bool ___playing_12;
	// System.Single[] DuelController::bounds
	SingleU5BU5D_t1444911251* ___bounds_13;
	// System.Single DuelController::halfwayPoint
	float ___halfwayPoint_14;

public:
	inline static int32_t get_offset_of_gameOverContainer_2() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___gameOverContainer_2)); }
	inline GameObject_t1113636619 * get_gameOverContainer_2() const { return ___gameOverContainer_2; }
	inline GameObject_t1113636619 ** get_address_of_gameOverContainer_2() { return &___gameOverContainer_2; }
	inline void set_gameOverContainer_2(GameObject_t1113636619 * value)
	{
		___gameOverContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverContainer_2), value);
	}

	inline static int32_t get_offset_of_divider_3() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___divider_3)); }
	inline GameObject_t1113636619 * get_divider_3() const { return ___divider_3; }
	inline GameObject_t1113636619 ** get_address_of_divider_3() { return &___divider_3; }
	inline void set_divider_3(GameObject_t1113636619 * value)
	{
		___divider_3 = value;
		Il2CppCodeGenWriteBarrier((&___divider_3), value);
	}

	inline static int32_t get_offset_of_bgMusic_4() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___bgMusic_4)); }
	inline AudioSource_t3935305588 * get_bgMusic_4() const { return ___bgMusic_4; }
	inline AudioSource_t3935305588 ** get_address_of_bgMusic_4() { return &___bgMusic_4; }
	inline void set_bgMusic_4(AudioSource_t3935305588 * value)
	{
		___bgMusic_4 = value;
		Il2CppCodeGenWriteBarrier((&___bgMusic_4), value);
	}

	inline static int32_t get_offset_of_failSound_5() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___failSound_5)); }
	inline AudioSource_t3935305588 * get_failSound_5() const { return ___failSound_5; }
	inline AudioSource_t3935305588 ** get_address_of_failSound_5() { return &___failSound_5; }
	inline void set_failSound_5(AudioSource_t3935305588 * value)
	{
		___failSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___failSound_5), value);
	}

	inline static int32_t get_offset_of_safeSounds_6() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___safeSounds_6)); }
	inline AudioSourceU5BU5D_t4028559421* get_safeSounds_6() const { return ___safeSounds_6; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_safeSounds_6() { return &___safeSounds_6; }
	inline void set_safeSounds_6(AudioSourceU5BU5D_t4028559421* value)
	{
		___safeSounds_6 = value;
		Il2CppCodeGenWriteBarrier((&___safeSounds_6), value);
	}

	inline static int32_t get_offset_of_panelContainer_7() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___panelContainer_7)); }
	inline GameObject_t1113636619 * get_panelContainer_7() const { return ___panelContainer_7; }
	inline GameObject_t1113636619 ** get_address_of_panelContainer_7() { return &___panelContainer_7; }
	inline void set_panelContainer_7(GameObject_t1113636619 * value)
	{
		___panelContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_7), value);
	}

	inline static int32_t get_offset_of_panelSprite_8() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___panelSprite_8)); }
	inline Sprite_t280657092 * get_panelSprite_8() const { return ___panelSprite_8; }
	inline Sprite_t280657092 ** get_address_of_panelSprite_8() { return &___panelSprite_8; }
	inline void set_panelSprite_8(Sprite_t280657092 * value)
	{
		___panelSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprite_8), value);
	}

	inline static int32_t get_offset_of_currentPanel_9() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___currentPanel_9)); }
	inline PanelController_t1280165360 * get_currentPanel_9() const { return ___currentPanel_9; }
	inline PanelController_t1280165360 ** get_address_of_currentPanel_9() { return &___currentPanel_9; }
	inline void set_currentPanel_9(PanelController_t1280165360 * value)
	{
		___currentPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanel_9), value);
	}

	inline static int32_t get_offset_of_colorScheme_10() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___colorScheme_10)); }
	inline int32_t get_colorScheme_10() const { return ___colorScheme_10; }
	inline int32_t* get_address_of_colorScheme_10() { return &___colorScheme_10; }
	inline void set_colorScheme_10(int32_t value)
	{
		___colorScheme_10 = value;
	}

	inline static int32_t get_offset_of_dir_11() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___dir_11)); }
	inline int32_t get_dir_11() const { return ___dir_11; }
	inline int32_t* get_address_of_dir_11() { return &___dir_11; }
	inline void set_dir_11(int32_t value)
	{
		___dir_11 = value;
	}

	inline static int32_t get_offset_of_playing_12() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___playing_12)); }
	inline bool get_playing_12() const { return ___playing_12; }
	inline bool* get_address_of_playing_12() { return &___playing_12; }
	inline void set_playing_12(bool value)
	{
		___playing_12 = value;
	}

	inline static int32_t get_offset_of_bounds_13() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___bounds_13)); }
	inline SingleU5BU5D_t1444911251* get_bounds_13() const { return ___bounds_13; }
	inline SingleU5BU5D_t1444911251** get_address_of_bounds_13() { return &___bounds_13; }
	inline void set_bounds_13(SingleU5BU5D_t1444911251* value)
	{
		___bounds_13 = value;
		Il2CppCodeGenWriteBarrier((&___bounds_13), value);
	}

	inline static int32_t get_offset_of_halfwayPoint_14() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___halfwayPoint_14)); }
	inline float get_halfwayPoint_14() const { return ___halfwayPoint_14; }
	inline float* get_address_of_halfwayPoint_14() { return &___halfwayPoint_14; }
	inline void set_halfwayPoint_14(float value)
	{
		___halfwayPoint_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUELCONTROLLER_T4245367713_H
#ifndef MENUPANELCONTROLLER_T3752352500_H
#define MENUPANELCONTROLLER_T3752352500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuPanelController
struct  MenuPanelController_t3752352500  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MenuPanelController::moving
	bool ___moving_2;
	// System.Single MenuPanelController::speed
	float ___speed_3;
	// System.Single MenuPanelController::maxHeight
	float ___maxHeight_4;
	// UnityEngine.RectTransform MenuPanelController::rectTransform
	RectTransform_t3704657025 * ___rectTransform_5;
	// System.Single MenuPanelController::delay
	float ___delay_6;
	// System.Single MenuPanelController::timer
	float ___timer_7;
	// System.Boolean MenuPanelController::finalized
	bool ___finalized_8;

public:
	inline static int32_t get_offset_of_moving_2() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___moving_2)); }
	inline bool get_moving_2() const { return ___moving_2; }
	inline bool* get_address_of_moving_2() { return &___moving_2; }
	inline void set_moving_2(bool value)
	{
		___moving_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_maxHeight_4() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___maxHeight_4)); }
	inline float get_maxHeight_4() const { return ___maxHeight_4; }
	inline float* get_address_of_maxHeight_4() { return &___maxHeight_4; }
	inline void set_maxHeight_4(float value)
	{
		___maxHeight_4 = value;
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___rectTransform_5)); }
	inline RectTransform_t3704657025 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t3704657025 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}

	inline static int32_t get_offset_of_delay_6() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___delay_6)); }
	inline float get_delay_6() const { return ___delay_6; }
	inline float* get_address_of_delay_6() { return &___delay_6; }
	inline void set_delay_6(float value)
	{
		___delay_6 = value;
	}

	inline static int32_t get_offset_of_timer_7() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___timer_7)); }
	inline float get_timer_7() const { return ___timer_7; }
	inline float* get_address_of_timer_7() { return &___timer_7; }
	inline void set_timer_7(float value)
	{
		___timer_7 = value;
	}

	inline static int32_t get_offset_of_finalized_8() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___finalized_8)); }
	inline bool get_finalized_8() const { return ___finalized_8; }
	inline bool* get_address_of_finalized_8() { return &___finalized_8; }
	inline void set_finalized_8(bool value)
	{
		___finalized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUPANELCONTROLLER_T3752352500_H
#ifndef PANELCONTAINERCONTROLLER_T2722594607_H
#define PANELCONTAINERCONTROLLER_T2722594607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelContainerController
struct  PanelContainerController_t2722594607  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PanelContainerController::shrinking
	bool ___shrinking_2;
	// UnityEngine.Vector3 PanelContainerController::startScale
	Vector3_t3722313464  ___startScale_3;
	// UnityEngine.Vector3 PanelContainerController::endScale
	Vector3_t3722313464  ___endScale_4;
	// System.Single PanelContainerController::lerpTimer
	float ___lerpTimer_5;
	// System.Single PanelContainerController::shrinkSpeed
	float ___shrinkSpeed_6;
	// System.Single PanelContainerController::screenshotCountdown
	float ___screenshotCountdown_7;
	// System.Boolean PanelContainerController::takingScreenshot
	bool ___takingScreenshot_8;
	// UnityEngine.GameObject[] PanelContainerController::hiddenForScreenshot
	GameObjectU5BU5D_t3328599146* ___hiddenForScreenshot_9;

public:
	inline static int32_t get_offset_of_shrinking_2() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___shrinking_2)); }
	inline bool get_shrinking_2() const { return ___shrinking_2; }
	inline bool* get_address_of_shrinking_2() { return &___shrinking_2; }
	inline void set_shrinking_2(bool value)
	{
		___shrinking_2 = value;
	}

	inline static int32_t get_offset_of_startScale_3() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___startScale_3)); }
	inline Vector3_t3722313464  get_startScale_3() const { return ___startScale_3; }
	inline Vector3_t3722313464 * get_address_of_startScale_3() { return &___startScale_3; }
	inline void set_startScale_3(Vector3_t3722313464  value)
	{
		___startScale_3 = value;
	}

	inline static int32_t get_offset_of_endScale_4() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___endScale_4)); }
	inline Vector3_t3722313464  get_endScale_4() const { return ___endScale_4; }
	inline Vector3_t3722313464 * get_address_of_endScale_4() { return &___endScale_4; }
	inline void set_endScale_4(Vector3_t3722313464  value)
	{
		___endScale_4 = value;
	}

	inline static int32_t get_offset_of_lerpTimer_5() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___lerpTimer_5)); }
	inline float get_lerpTimer_5() const { return ___lerpTimer_5; }
	inline float* get_address_of_lerpTimer_5() { return &___lerpTimer_5; }
	inline void set_lerpTimer_5(float value)
	{
		___lerpTimer_5 = value;
	}

	inline static int32_t get_offset_of_shrinkSpeed_6() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___shrinkSpeed_6)); }
	inline float get_shrinkSpeed_6() const { return ___shrinkSpeed_6; }
	inline float* get_address_of_shrinkSpeed_6() { return &___shrinkSpeed_6; }
	inline void set_shrinkSpeed_6(float value)
	{
		___shrinkSpeed_6 = value;
	}

	inline static int32_t get_offset_of_screenshotCountdown_7() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___screenshotCountdown_7)); }
	inline float get_screenshotCountdown_7() const { return ___screenshotCountdown_7; }
	inline float* get_address_of_screenshotCountdown_7() { return &___screenshotCountdown_7; }
	inline void set_screenshotCountdown_7(float value)
	{
		___screenshotCountdown_7 = value;
	}

	inline static int32_t get_offset_of_takingScreenshot_8() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___takingScreenshot_8)); }
	inline bool get_takingScreenshot_8() const { return ___takingScreenshot_8; }
	inline bool* get_address_of_takingScreenshot_8() { return &___takingScreenshot_8; }
	inline void set_takingScreenshot_8(bool value)
	{
		___takingScreenshot_8 = value;
	}

	inline static int32_t get_offset_of_hiddenForScreenshot_9() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___hiddenForScreenshot_9)); }
	inline GameObjectU5BU5D_t3328599146* get_hiddenForScreenshot_9() const { return ___hiddenForScreenshot_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_hiddenForScreenshot_9() { return &___hiddenForScreenshot_9; }
	inline void set_hiddenForScreenshot_9(GameObjectU5BU5D_t3328599146* value)
	{
		___hiddenForScreenshot_9 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenForScreenshot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELCONTAINERCONTROLLER_T2722594607_H
#ifndef SQUARECONTROLLER_T2553685805_H
#define SQUARECONTROLLER_T2553685805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SquareController
struct  SquareController_t2553685805  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SquareController::highScore
	int32_t ___highScore_2;
	// System.Boolean SquareController::setHighScore
	bool ___setHighScore_3;
	// UnityEngine.UI.Text SquareController::scoreText
	Text_t1901882714 * ___scoreText_4;
	// UnityEngine.GameObject SquareController::gameOverContainer
	GameObject_t1113636619 * ___gameOverContainer_5;
	// UnityEngine.UI.Text SquareController::gameOverScoreText
	Text_t1901882714 * ___gameOverScoreText_6;
	// UnityEngine.UI.Text SquareController::gameOverRecordText
	Text_t1901882714 * ___gameOverRecordText_7;
	// UnityEngine.UI.Text SquareController::newThemeText
	Text_t1901882714 * ___newThemeText_8;
	// UnityEngine.GameObject SquareController::exportButton
	GameObject_t1113636619 * ___exportButton_9;
	// UnityEngine.AudioSource SquareController::bgMusic
	AudioSource_t3935305588 * ___bgMusic_10;
	// UnityEngine.AudioSource SquareController::drums
	AudioSource_t3935305588 * ___drums_11;
	// UnityEngine.AudioSource SquareController::applause
	AudioSource_t3935305588 * ___applause_12;
	// UnityEngine.AudioSource[] SquareController::accentSounds
	AudioSourceU5BU5D_t4028559421* ___accentSounds_13;
	// UnityEngine.AudioSource SquareController::failSound
	AudioSource_t3935305588 * ___failSound_14;
	// UnityEngine.AudioSource[] SquareController::safeSounds
	AudioSourceU5BU5D_t4028559421* ___safeSounds_15;
	// UnityEngine.GameObject SquareController::panelContainer
	GameObject_t1113636619 * ___panelContainer_16;
	// UnityEngine.GameObject SquareController::allPanelsContainer
	GameObject_t1113636619 * ___allPanelsContainer_17;
	// UnityEngine.Sprite SquareController::panelSprite
	Sprite_t280657092 * ___panelSprite_18;
	// PanelController SquareController::currentPanel
	PanelController_t1280165360 * ___currentPanel_19;
	// System.Int32 SquareController::colorScheme
	int32_t ___colorScheme_20;
	// System.Int32 SquareController::dir
	int32_t ___dir_21;
	// System.Int32 SquareController::score
	int32_t ___score_22;
	// System.Boolean SquareController::playing
	bool ___playing_23;
	// System.Single[] SquareController::bounds
	SingleU5BU5D_t1444911251* ___bounds_24;
	// System.Int32 SquareController::numThemesUnlocked
	int32_t ___numThemesUnlocked_25;
	// System.Boolean SquareController::unlockedATheme
	bool ___unlockedATheme_26;
	// FadeOutController SquareController::gameOverFade
	FadeOutController_t1417150664 * ___gameOverFade_27;

public:
	inline static int32_t get_offset_of_highScore_2() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___highScore_2)); }
	inline int32_t get_highScore_2() const { return ___highScore_2; }
	inline int32_t* get_address_of_highScore_2() { return &___highScore_2; }
	inline void set_highScore_2(int32_t value)
	{
		___highScore_2 = value;
	}

	inline static int32_t get_offset_of_setHighScore_3() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___setHighScore_3)); }
	inline bool get_setHighScore_3() const { return ___setHighScore_3; }
	inline bool* get_address_of_setHighScore_3() { return &___setHighScore_3; }
	inline void set_setHighScore_3(bool value)
	{
		___setHighScore_3 = value;
	}

	inline static int32_t get_offset_of_scoreText_4() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___scoreText_4)); }
	inline Text_t1901882714 * get_scoreText_4() const { return ___scoreText_4; }
	inline Text_t1901882714 ** get_address_of_scoreText_4() { return &___scoreText_4; }
	inline void set_scoreText_4(Text_t1901882714 * value)
	{
		___scoreText_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_4), value);
	}

	inline static int32_t get_offset_of_gameOverContainer_5() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverContainer_5)); }
	inline GameObject_t1113636619 * get_gameOverContainer_5() const { return ___gameOverContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_gameOverContainer_5() { return &___gameOverContainer_5; }
	inline void set_gameOverContainer_5(GameObject_t1113636619 * value)
	{
		___gameOverContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverContainer_5), value);
	}

	inline static int32_t get_offset_of_gameOverScoreText_6() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverScoreText_6)); }
	inline Text_t1901882714 * get_gameOverScoreText_6() const { return ___gameOverScoreText_6; }
	inline Text_t1901882714 ** get_address_of_gameOverScoreText_6() { return &___gameOverScoreText_6; }
	inline void set_gameOverScoreText_6(Text_t1901882714 * value)
	{
		___gameOverScoreText_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScoreText_6), value);
	}

	inline static int32_t get_offset_of_gameOverRecordText_7() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverRecordText_7)); }
	inline Text_t1901882714 * get_gameOverRecordText_7() const { return ___gameOverRecordText_7; }
	inline Text_t1901882714 ** get_address_of_gameOverRecordText_7() { return &___gameOverRecordText_7; }
	inline void set_gameOverRecordText_7(Text_t1901882714 * value)
	{
		___gameOverRecordText_7 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverRecordText_7), value);
	}

	inline static int32_t get_offset_of_newThemeText_8() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___newThemeText_8)); }
	inline Text_t1901882714 * get_newThemeText_8() const { return ___newThemeText_8; }
	inline Text_t1901882714 ** get_address_of_newThemeText_8() { return &___newThemeText_8; }
	inline void set_newThemeText_8(Text_t1901882714 * value)
	{
		___newThemeText_8 = value;
		Il2CppCodeGenWriteBarrier((&___newThemeText_8), value);
	}

	inline static int32_t get_offset_of_exportButton_9() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___exportButton_9)); }
	inline GameObject_t1113636619 * get_exportButton_9() const { return ___exportButton_9; }
	inline GameObject_t1113636619 ** get_address_of_exportButton_9() { return &___exportButton_9; }
	inline void set_exportButton_9(GameObject_t1113636619 * value)
	{
		___exportButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___exportButton_9), value);
	}

	inline static int32_t get_offset_of_bgMusic_10() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___bgMusic_10)); }
	inline AudioSource_t3935305588 * get_bgMusic_10() const { return ___bgMusic_10; }
	inline AudioSource_t3935305588 ** get_address_of_bgMusic_10() { return &___bgMusic_10; }
	inline void set_bgMusic_10(AudioSource_t3935305588 * value)
	{
		___bgMusic_10 = value;
		Il2CppCodeGenWriteBarrier((&___bgMusic_10), value);
	}

	inline static int32_t get_offset_of_drums_11() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___drums_11)); }
	inline AudioSource_t3935305588 * get_drums_11() const { return ___drums_11; }
	inline AudioSource_t3935305588 ** get_address_of_drums_11() { return &___drums_11; }
	inline void set_drums_11(AudioSource_t3935305588 * value)
	{
		___drums_11 = value;
		Il2CppCodeGenWriteBarrier((&___drums_11), value);
	}

	inline static int32_t get_offset_of_applause_12() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___applause_12)); }
	inline AudioSource_t3935305588 * get_applause_12() const { return ___applause_12; }
	inline AudioSource_t3935305588 ** get_address_of_applause_12() { return &___applause_12; }
	inline void set_applause_12(AudioSource_t3935305588 * value)
	{
		___applause_12 = value;
		Il2CppCodeGenWriteBarrier((&___applause_12), value);
	}

	inline static int32_t get_offset_of_accentSounds_13() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___accentSounds_13)); }
	inline AudioSourceU5BU5D_t4028559421* get_accentSounds_13() const { return ___accentSounds_13; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_accentSounds_13() { return &___accentSounds_13; }
	inline void set_accentSounds_13(AudioSourceU5BU5D_t4028559421* value)
	{
		___accentSounds_13 = value;
		Il2CppCodeGenWriteBarrier((&___accentSounds_13), value);
	}

	inline static int32_t get_offset_of_failSound_14() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___failSound_14)); }
	inline AudioSource_t3935305588 * get_failSound_14() const { return ___failSound_14; }
	inline AudioSource_t3935305588 ** get_address_of_failSound_14() { return &___failSound_14; }
	inline void set_failSound_14(AudioSource_t3935305588 * value)
	{
		___failSound_14 = value;
		Il2CppCodeGenWriteBarrier((&___failSound_14), value);
	}

	inline static int32_t get_offset_of_safeSounds_15() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___safeSounds_15)); }
	inline AudioSourceU5BU5D_t4028559421* get_safeSounds_15() const { return ___safeSounds_15; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_safeSounds_15() { return &___safeSounds_15; }
	inline void set_safeSounds_15(AudioSourceU5BU5D_t4028559421* value)
	{
		___safeSounds_15 = value;
		Il2CppCodeGenWriteBarrier((&___safeSounds_15), value);
	}

	inline static int32_t get_offset_of_panelContainer_16() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___panelContainer_16)); }
	inline GameObject_t1113636619 * get_panelContainer_16() const { return ___panelContainer_16; }
	inline GameObject_t1113636619 ** get_address_of_panelContainer_16() { return &___panelContainer_16; }
	inline void set_panelContainer_16(GameObject_t1113636619 * value)
	{
		___panelContainer_16 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_16), value);
	}

	inline static int32_t get_offset_of_allPanelsContainer_17() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___allPanelsContainer_17)); }
	inline GameObject_t1113636619 * get_allPanelsContainer_17() const { return ___allPanelsContainer_17; }
	inline GameObject_t1113636619 ** get_address_of_allPanelsContainer_17() { return &___allPanelsContainer_17; }
	inline void set_allPanelsContainer_17(GameObject_t1113636619 * value)
	{
		___allPanelsContainer_17 = value;
		Il2CppCodeGenWriteBarrier((&___allPanelsContainer_17), value);
	}

	inline static int32_t get_offset_of_panelSprite_18() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___panelSprite_18)); }
	inline Sprite_t280657092 * get_panelSprite_18() const { return ___panelSprite_18; }
	inline Sprite_t280657092 ** get_address_of_panelSprite_18() { return &___panelSprite_18; }
	inline void set_panelSprite_18(Sprite_t280657092 * value)
	{
		___panelSprite_18 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprite_18), value);
	}

	inline static int32_t get_offset_of_currentPanel_19() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___currentPanel_19)); }
	inline PanelController_t1280165360 * get_currentPanel_19() const { return ___currentPanel_19; }
	inline PanelController_t1280165360 ** get_address_of_currentPanel_19() { return &___currentPanel_19; }
	inline void set_currentPanel_19(PanelController_t1280165360 * value)
	{
		___currentPanel_19 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanel_19), value);
	}

	inline static int32_t get_offset_of_colorScheme_20() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___colorScheme_20)); }
	inline int32_t get_colorScheme_20() const { return ___colorScheme_20; }
	inline int32_t* get_address_of_colorScheme_20() { return &___colorScheme_20; }
	inline void set_colorScheme_20(int32_t value)
	{
		___colorScheme_20 = value;
	}

	inline static int32_t get_offset_of_dir_21() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___dir_21)); }
	inline int32_t get_dir_21() const { return ___dir_21; }
	inline int32_t* get_address_of_dir_21() { return &___dir_21; }
	inline void set_dir_21(int32_t value)
	{
		___dir_21 = value;
	}

	inline static int32_t get_offset_of_score_22() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___score_22)); }
	inline int32_t get_score_22() const { return ___score_22; }
	inline int32_t* get_address_of_score_22() { return &___score_22; }
	inline void set_score_22(int32_t value)
	{
		___score_22 = value;
	}

	inline static int32_t get_offset_of_playing_23() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___playing_23)); }
	inline bool get_playing_23() const { return ___playing_23; }
	inline bool* get_address_of_playing_23() { return &___playing_23; }
	inline void set_playing_23(bool value)
	{
		___playing_23 = value;
	}

	inline static int32_t get_offset_of_bounds_24() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___bounds_24)); }
	inline SingleU5BU5D_t1444911251* get_bounds_24() const { return ___bounds_24; }
	inline SingleU5BU5D_t1444911251** get_address_of_bounds_24() { return &___bounds_24; }
	inline void set_bounds_24(SingleU5BU5D_t1444911251* value)
	{
		___bounds_24 = value;
		Il2CppCodeGenWriteBarrier((&___bounds_24), value);
	}

	inline static int32_t get_offset_of_numThemesUnlocked_25() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___numThemesUnlocked_25)); }
	inline int32_t get_numThemesUnlocked_25() const { return ___numThemesUnlocked_25; }
	inline int32_t* get_address_of_numThemesUnlocked_25() { return &___numThemesUnlocked_25; }
	inline void set_numThemesUnlocked_25(int32_t value)
	{
		___numThemesUnlocked_25 = value;
	}

	inline static int32_t get_offset_of_unlockedATheme_26() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___unlockedATheme_26)); }
	inline bool get_unlockedATheme_26() const { return ___unlockedATheme_26; }
	inline bool* get_address_of_unlockedATheme_26() { return &___unlockedATheme_26; }
	inline void set_unlockedATheme_26(bool value)
	{
		___unlockedATheme_26 = value;
	}

	inline static int32_t get_offset_of_gameOverFade_27() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverFade_27)); }
	inline FadeOutController_t1417150664 * get_gameOverFade_27() const { return ___gameOverFade_27; }
	inline FadeOutController_t1417150664 ** get_address_of_gameOverFade_27() { return &___gameOverFade_27; }
	inline void set_gameOverFade_27(FadeOutController_t1417150664 * value)
	{
		___gameOverFade_27 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverFade_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQUARECONTROLLER_T2553685805_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t941916414  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Color_t2555686324  value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Color_t2555686324  value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t1444911251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t4028559421  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioSource_t3935305588 * m_Items[1];

public:
	inline AudioSource_t3935305588 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioSource_t3935305588 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioSource_t3935305588 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AudioSource_t3935305588 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioSource_t3935305588 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioSource_t3935305588 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single PanelController::StopMoving()
extern "C"  float PanelController_StopMoving_m198065155 (PanelController_t1280165360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DuelController::GameOver(System.Int32)
extern "C"  void DuelController_GameOver_m638072373 (DuelController_t4245367713 * __this, int32_t ___winner0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DuelController::PlaySafeSound()
extern "C"  void DuelController_PlaySafeSound_m458390668 (DuelController_t4245367713 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DuelController::CreatePanel()
extern "C"  void DuelController_CreatePanel_m1900430364 (DuelController_t4245367713 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.CanvasRenderer>()
#define GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522(__this, method) ((  CanvasRenderer_t2598313366 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Image>()
#define GameObject_AddComponent_TisImage_t2670269651_m1594579417(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DuelController::SetPanelAnchor(UnityEngine.GameObject,System.Int32)
extern "C"  void DuelController_SetPanelAnchor_m3823622997 (DuelController_t4245367713 * __this, GameObject_t1113636619 * ___panel0, int32_t ___dir1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m273471670 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_AddComponent_m136524825 (GameObject_t1113636619 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m909387058 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4230103102 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2998668828 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m2682712816 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m720511863 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern "C"  Color_t2555686324  Color_op_Implicit_m2665280078 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuScript::SetTheme(System.Int32)
extern "C"  void MainMenuScript_SetTheme_m1616100854 (MainMenuScript_t2493777029 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuScript::SetUpColorSelectionMenu(System.Int32)
extern "C"  void MainMenuScript_SetUpColorSelectionMenu_m2362659385 (MainMenuScript_t2493777029 * __this, int32_t ___numThemes0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2670269651_m2486712510(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<MenuColorChoiceController>()
#define GameObject_GetComponent_TisMenuColorChoiceController_t2734251472_m102361087(__this, method) ((  MenuColorChoiceController_t2734251472 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void MenuColorChoiceController::SetThemeIndex(System.Int32)
extern "C"  void MenuColorChoiceController_SetThemeIndex_m2347622676 (MenuColorChoiceController_t2734251472 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<MainMenuScript>()
#define GameObject_GetComponent_TisMainMenuScript_t2493777029_m3991380284(__this, method) ((  MainMenuScript_t2493777029 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2183112744 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MenuPanelController::StartMoving()
extern "C"  void MenuPanelController_StartMoving_m3728774834 (MenuPanelController_t3752352500 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String)
extern "C"  void ScreenCapture_CaptureScreenshot_m934745658 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::CreateLeadingEdge()
extern "C"  void PanelController_CreateLeadingEdge_m1745492603 (PanelController_t1280165360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::Move()
extern "C"  void PanelController_Move_m1423420284 (PanelController_t1280165360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::SetLeadingEdgeAnchor()
extern "C"  void PanelController_SetLeadingEdgeAnchor_m2694989816 (PanelController_t1280165360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PanelController::GameOver()
extern "C"  void PanelController_GameOver_m2942013208 (PanelController_t1280165360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::GameOver()
extern "C"  void SquareController_GameOver_m1811381354 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::PlaySafeSound()
extern "C"  void SquareController_PlaySafeSound_m273964245 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::IncreaseScore()
extern "C"  void SquareController_IncreaseScore_m3643553335 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::PlayAccentSound()
extern "C"  void SquareController_PlayAccentSound_m1646413189 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::CreatePanel()
extern "C"  void SquareController_CreatePanel_m2690008112 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::SetPanelAnchor(UnityEngine.GameObject,System.Int32)
extern "C"  void SquareController_SetPanelAnchor_m2372856663 (SquareController_t2553685805 * __this, GameObject_t1113636619 * ___panel0, int32_t ___dir1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::Applaud()
extern "C"  void SquareController_Applaud_m2287170021 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::UnlockTheme()
extern "C"  void SquareController_UnlockTheme_m880230570 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PanelContainerController>()
#define GameObject_GetComponent_TisPanelContainerController_t2722594607_m3234714783(__this, method) ((  PanelContainerController_t2722594607 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void PanelContainerController::StartShrinking()
extern "C"  void PanelContainerController_StartShrinking_m3156969907 (PanelContainerController_t2722594607 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SquareController::UpdateGameOverGUI()
extern "C"  void SquareController_UpdateGameOverGUI_m959589948 (SquareController_t2553685805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void FadeOutController::StartFading()
extern "C"  void FadeOutController_StartFading_m888056018 (FadeOutController_t1417150664 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLength(System.Int32)
extern "C"  int32_t Array_GetLength_m2178203778 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ColorSchemeManager::.ctor()
extern "C"  void ColorSchemeManager__ctor_m1292507336 (ColorSchemeManager_t1974266384 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorSchemeManager::.cctor()
extern "C"  void ColorSchemeManager__cctor_m3585789103 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSchemeManager__cctor_m3585789103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)((int32_t)10), (il2cpp_array_size_t)4 };
		ColorU5B0___U2C0___U5D_t941916414* L_0 = (ColorU5B0___U2C0___U5D_t941916414*)GenArrayNew(ColorU5B0___U2C0___U5D_t941916414_il2cpp_TypeInfo_var, L_1);
		ColorU5B0___U2C0___U5D_t941916414* L_2 = L_0;
		Color_t2555686324  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m2943235014((&L_3), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(0, 0, L_3);
		ColorU5B0___U2C0___U5D_t941916414* L_4 = L_2;
		Color_t2555686324  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m2943235014((&L_5), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(0, 1, L_5);
		ColorU5B0___U2C0___U5D_t941916414* L_6 = L_4;
		Color_t2555686324  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2943235014((&L_7), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(0, 2, L_7);
		ColorU5B0___U2C0___U5D_t941916414* L_8 = L_6;
		Color_t2555686324  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m2943235014((&L_9), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(0, 3, L_9);
		ColorU5B0___U2C0___U5D_t941916414* L_10 = L_8;
		Color_t2555686324  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m2943235014((&L_11), (0.93f), (0.19f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_10);
		(L_10)->SetAt(1, 0, L_11);
		ColorU5B0___U2C0___U5D_t941916414* L_12 = L_10;
		Color_t2555686324  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m2943235014((&L_13), (0.93f), (0.19f), (0.85f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_12);
		(L_12)->SetAt(1, 1, L_13);
		ColorU5B0___U2C0___U5D_t941916414* L_14 = L_12;
		Color_t2555686324  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m2943235014((&L_15), (0.43f), (0.19f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_14);
		(L_14)->SetAt(1, 2, L_15);
		ColorU5B0___U2C0___U5D_t941916414* L_16 = L_14;
		Color_t2555686324  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m2943235014((&L_17), (0.19f), (0.75f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_16);
		(L_16)->SetAt(1, 3, L_17);
		ColorU5B0___U2C0___U5D_t941916414* L_18 = L_16;
		Color_t2555686324  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Color__ctor_m2943235014((&L_19), (0.93f), (0.19f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_18);
		(L_18)->SetAt(2, 0, L_19);
		ColorU5B0___U2C0___U5D_t941916414* L_20 = L_18;
		Color_t2555686324  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Color__ctor_m2943235014((&L_21), (0.19f), (0.93f), (0.53f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_20);
		(L_20)->SetAt(2, 1, L_21);
		ColorU5B0___U2C0___U5D_t941916414* L_22 = L_20;
		Color_t2555686324  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Color__ctor_m2943235014((&L_23), (0.19f), (0.31f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_22);
		(L_22)->SetAt(2, 2, L_23);
		ColorU5B0___U2C0___U5D_t941916414* L_24 = L_22;
		Color_t2555686324  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Color__ctor_m2943235014((&L_25), (0.93f), (0.93f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(2, 3, L_25);
		ColorU5B0___U2C0___U5D_t941916414* L_26 = L_24;
		Color_t2555686324  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Color__ctor_m2943235014((&L_27), (0.93f), (0.53f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(3, 0, L_27);
		ColorU5B0___U2C0___U5D_t941916414* L_28 = L_26;
		Color_t2555686324  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Color__ctor_m2943235014((&L_29), (0.93f), (0.37f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_28);
		(L_28)->SetAt(3, 1, L_29);
		ColorU5B0___U2C0___U5D_t941916414* L_30 = L_28;
		Color_t2555686324  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Color__ctor_m2943235014((&L_31), (0.85f), (0.93f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_30);
		(L_30)->SetAt(3, 2, L_31);
		ColorU5B0___U2C0___U5D_t941916414* L_32 = L_30;
		Color_t2555686324  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Color__ctor_m2943235014((&L_33), (0.93f), (0.19f), (0.31f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_32);
		(L_32)->SetAt(3, 3, L_33);
		ColorU5B0___U2C0___U5D_t941916414* L_34 = L_32;
		Color_t2555686324  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Color__ctor_m2943235014((&L_35), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_34);
		(L_34)->SetAt(4, 0, L_35);
		ColorU5B0___U2C0___U5D_t941916414* L_36 = L_34;
		Color_t2555686324  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Color__ctor_m2943235014((&L_37), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_36);
		(L_36)->SetAt(4, 1, L_37);
		ColorU5B0___U2C0___U5D_t941916414* L_38 = L_36;
		Color_t2555686324  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Color__ctor_m2943235014((&L_39), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_38);
		(L_38)->SetAt(4, 2, L_39);
		ColorU5B0___U2C0___U5D_t941916414* L_40 = L_38;
		Color_t2555686324  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Color__ctor_m2943235014((&L_41), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_40);
		(L_40)->SetAt(4, 3, L_41);
		ColorU5B0___U2C0___U5D_t941916414* L_42 = L_40;
		Color_t2555686324  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Color__ctor_m2943235014((&L_43), (0.93f), (0.19f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_42);
		(L_42)->SetAt(5, 0, L_43);
		ColorU5B0___U2C0___U5D_t941916414* L_44 = L_42;
		Color_t2555686324  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Color__ctor_m2943235014((&L_45), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_44);
		(L_44)->SetAt(5, 1, L_45);
		ColorU5B0___U2C0___U5D_t941916414* L_46 = L_44;
		Color_t2555686324  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Color__ctor_m2943235014((&L_47), (0.93f), (0.19f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_46);
		(L_46)->SetAt(5, 2, L_47);
		ColorU5B0___U2C0___U5D_t941916414* L_48 = L_46;
		Color_t2555686324  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Color__ctor_m2943235014((&L_49), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_48);
		(L_48)->SetAt(5, 3, L_49);
		ColorU5B0___U2C0___U5D_t941916414* L_50 = L_48;
		Color_t2555686324  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Color__ctor_m2943235014((&L_51), (0.93f), (0.53f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_50);
		(L_50)->SetAt(6, 0, L_51);
		ColorU5B0___U2C0___U5D_t941916414* L_52 = L_50;
		Color_t2555686324  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Color__ctor_m2943235014((&L_53), (0.93f), (0.53f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_52);
		(L_52)->SetAt(6, 1, L_53);
		ColorU5B0___U2C0___U5D_t941916414* L_54 = L_52;
		Color_t2555686324  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Color__ctor_m2943235014((&L_55), (0.0f), (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_54);
		(L_54)->SetAt(6, 2, L_55);
		ColorU5B0___U2C0___U5D_t941916414* L_56 = L_54;
		Color_t2555686324  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Color__ctor_m2943235014((&L_57), (0.93f), (0.53f), (0.19f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_56);
		(L_56)->SetAt(6, 3, L_57);
		ColorU5B0___U2C0___U5D_t941916414* L_58 = L_56;
		Color_t2555686324  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Color__ctor_m2943235014((&L_59), (0.19f), (0.75f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_58);
		(L_58)->SetAt(7, 0, L_59);
		ColorU5B0___U2C0___U5D_t941916414* L_60 = L_58;
		Color_t2555686324  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Color__ctor_m2943235014((&L_61), (0.19f), (0.75f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_60);
		(L_60)->SetAt(7, 1, L_61);
		ColorU5B0___U2C0___U5D_t941916414* L_62 = L_60;
		Color_t2555686324  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Color__ctor_m2943235014((&L_63), (0.19f), (0.75f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_62);
		(L_62)->SetAt(7, 2, L_63);
		ColorU5B0___U2C0___U5D_t941916414* L_64 = L_62;
		Color_t2555686324  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Color__ctor_m2943235014((&L_65), (0.19f), (0.75f), (0.93f), (0.25f), /*hidden argument*/NULL);
		NullCheck(L_64);
		(L_64)->SetAt(7, 3, L_65);
		ColorU5B0___U2C0___U5D_t941916414* L_66 = L_64;
		Color_t2555686324  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Color__ctor_m2943235014((&L_67), (0.0f), (0.0f), (0.0f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_66);
		(L_66)->SetAt(8, 0, L_67);
		ColorU5B0___U2C0___U5D_t941916414* L_68 = L_66;
		Color_t2555686324  L_69;
		memset(&L_69, 0, sizeof(L_69));
		Color__ctor_m2943235014((&L_69), (0.0f), (0.0f), (0.0f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_68);
		(L_68)->SetAt(8, 1, L_69);
		ColorU5B0___U2C0___U5D_t941916414* L_70 = L_68;
		Color_t2555686324  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Color__ctor_m2943235014((&L_71), (0.0f), (0.0f), (0.0f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_70);
		(L_70)->SetAt(8, 2, L_71);
		ColorU5B0___U2C0___U5D_t941916414* L_72 = L_70;
		Color_t2555686324  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Color__ctor_m2943235014((&L_73), (0.0f), (0.0f), (0.0f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_72);
		(L_72)->SetAt(8, 3, L_73);
		ColorU5B0___U2C0___U5D_t941916414* L_74 = L_72;
		Color_t2555686324  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Color__ctor_m2943235014((&L_75), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_74);
		(L_74)->SetAt(((int32_t)9), 0, L_75);
		ColorU5B0___U2C0___U5D_t941916414* L_76 = L_74;
		Color_t2555686324  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Color__ctor_m2943235014((&L_77), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_76);
		(L_76)->SetAt(((int32_t)9), 1, L_77);
		ColorU5B0___U2C0___U5D_t941916414* L_78 = L_76;
		Color_t2555686324  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Color__ctor_m2943235014((&L_79), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_78);
		(L_78)->SetAt(((int32_t)9), 2, L_79);
		ColorU5B0___U2C0___U5D_t941916414* L_80 = L_78;
		Color_t2555686324  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Color__ctor_m2943235014((&L_81), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_80);
		(L_80)->SetAt(((int32_t)9), 3, L_81);
		((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->set_colorSchemes_0((ColorU5B0___U2C0___U5D_t941916414*)L_80);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DuelController::.ctor()
extern "C"  void DuelController__ctor_m184767228 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController__ctor_m184767228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_playing_12((bool)1);
		__this->set_bounds_13(((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelController::Start()
extern "C"  void DuelController_Start_m2226500390 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController_Start_m2226500390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral1410563525, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2718991579, 0, /*hidden argument*/NULL);
		__this->set_colorScheme_10(L_1);
		int32_t L_2 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_halfwayPoint_14((((float)((float)((int32_t)((int32_t)L_2/(int32_t)2))))));
		SingleU5BU5D_t1444911251* L_3 = __this->get_bounds_13();
		float L_4 = __this->get_halfwayPoint_14();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)((float)il2cpp_codegen_subtract((float)L_4, (float)(5.0f))));
		SingleU5BU5D_t1444911251* L_5 = __this->get_bounds_13();
		float L_6 = __this->get_halfwayPoint_14();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)((float)il2cpp_codegen_subtract((float)L_6, (float)(5.0f))));
		return;
	}
}
// System.Void DuelController::Update()
extern "C"  void DuelController_Update_m3698156876 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController_Update_m3698156876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_playing_12();
		if (!L_0)
		{
			goto IL_00d3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		float L_4 = __this->get_halfwayPoint_14();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0066;
		}
	}
	{
		PanelController_t1280165360 * L_5 = __this->get_currentPanel_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0066;
		}
	}
	{
		PanelController_t1280165360 * L_7 = __this->get_currentPanel_9();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_dir_4();
		if (!L_8)
		{
			goto IL_0066;
		}
	}
	{
		PanelController_t1280165360 * L_9 = __this->get_currentPanel_9();
		NullCheck(L_9);
		PanelController_StopMoving_m198065155(L_9, /*hidden argument*/NULL);
		DuelController_GameOver_m638072373(__this, 1, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_y_2();
		float L_12 = __this->get_halfwayPoint_14();
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_00b7;
		}
	}
	{
		PanelController_t1280165360 * L_13 = __this->get_currentPanel_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b7;
		}
	}
	{
		PanelController_t1280165360 * L_15 = __this->get_currentPanel_9();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_dir_4();
		if ((((int32_t)L_16) == ((int32_t)2)))
		{
			goto IL_00b7;
		}
	}
	{
		PanelController_t1280165360 * L_17 = __this->get_currentPanel_9();
		NullCheck(L_17);
		PanelController_StopMoving_m198065155(L_17, /*hidden argument*/NULL);
		DuelController_GameOver_m638072373(__this, 0, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_00b7:
	{
		PanelController_t1280165360 * L_18 = __this->get_currentPanel_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00cd;
		}
	}
	{
		DuelController_PlaySafeSound_m458390668(__this, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		DuelController_CreatePanel_m1900430364(__this, /*hidden argument*/NULL);
	}

IL_00d3:
	{
		return;
	}
}
// System.Void DuelController::CreatePanel()
extern "C"  void DuelController_CreatePanel_m1900430364 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController_CreatePanel_m1900430364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	GameObject_t1113636619 * V_1 = NULL;
	Image_t2670269651 * V_2 = NULL;
	PanelController_t1280165360 * V_3 = NULL;
	{
		PanelController_t1280165360 * L_0 = __this->get_currentPanel_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009a;
		}
	}
	{
		PanelController_t1280165360 * L_2 = __this->get_currentPanel_9();
		NullCheck(L_2);
		float L_3 = PanelController_StopMoving_m198065155(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PanelController_t1280165360 * L_4 = __this->get_currentPanel_9();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_dir_4();
		if (L_5)
		{
			goto IL_0057;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_6 = __this->get_bounds_13();
		float L_7 = V_0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_7);
		SingleU5BU5D_t1444911251* L_8 = __this->get_bounds_13();
		SingleU5BU5D_t1444911251* L_9 = __this->get_bounds_13();
		NullCheck(L_9);
		int32_t L_10 = 2;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_14 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_11, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_12))), (float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_14);
		goto IL_009a;
	}

IL_0057:
	{
		PanelController_t1280165360 * L_15 = __this->get_currentPanel_9();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_dir_4();
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_009a;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_17 = __this->get_bounds_13();
		float L_18 = V_0;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_18);
		SingleU5BU5D_t1444911251* L_19 = __this->get_bounds_13();
		SingleU5BU5D_t1444911251* L_20 = __this->get_bounds_13();
		NullCheck(L_20);
		int32_t L_21 = 0;
		float L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_22, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_23))), (float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_25);
		GameObject_t1113636619 * L_26 = __this->get_divider_3();
		NullCheck(L_26);
		GameObject_SetActive_m796801857(L_26, (bool)0, /*hidden argument*/NULL);
	}

IL_009a:
	{
		GameObject_t1113636619 * L_27 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_27, _stringLiteral1597722606, /*hidden argument*/NULL);
		V_1 = L_27;
		GameObject_t1113636619 * L_28 = V_1;
		NullCheck(L_28);
		GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522(L_28, /*hidden argument*/GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522_RuntimeMethod_var);
		GameObject_t1113636619 * L_29 = V_1;
		NullCheck(L_29);
		Image_t2670269651 * L_30 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_29, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		V_2 = L_30;
		Image_t2670269651 * L_31 = V_2;
		Sprite_t280657092 * L_32 = __this->get_panelSprite_8();
		NullCheck(L_31);
		Image_set_sprite_m2369174689(L_31, L_32, /*hidden argument*/NULL);
		Image_t2670269651 * L_33 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_34 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_35 = __this->get_colorScheme_10();
		int32_t L_36 = __this->get_dir_11();
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_34);
		Color_t2555686324  L_37 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_34)->GetAt(L_35, L_36);
		NullCheck(L_33);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_33, L_37);
		GameObject_t1113636619 * L_38 = V_1;
		int32_t L_39 = __this->get_dir_11();
		DuelController_SetPanelAnchor_m3823622997(__this, L_38, L_39, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_40 = V_1;
		NullCheck(L_40);
		Transform_t3600365921 * L_41 = GameObject_get_transform_m1369836730(L_40, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_42 = __this->get_panelContainer_7();
		NullCheck(L_42);
		Transform_t3600365921 * L_43 = GameObject_get_transform_m1369836730(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_SetParent_m273471670(L_41, L_43, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_44 = V_1;
		RuntimeTypeHandle_t3027515415  L_45 = { reinterpret_cast<intptr_t> (PanelController_t1280165360_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Component_t1923634451 * L_47 = GameObject_AddComponent_m136524825(L_44, L_46, /*hidden argument*/NULL);
		V_3 = ((PanelController_t1280165360 *)IsInstClass((RuntimeObject*)L_47, PanelController_t1280165360_il2cpp_TypeInfo_var));
		PanelController_t1280165360 * L_48 = V_3;
		Sprite_t280657092 * L_49 = __this->get_panelSprite_8();
		NullCheck(L_48);
		L_48->set_leadingEdgeSprite_10(L_49);
		PanelController_t1280165360 * L_50 = V_3;
		int32_t L_51 = __this->get_dir_11();
		NullCheck(L_50);
		L_50->set_dir_4(L_51);
		PanelController_t1280165360 * L_52 = V_3;
		SingleU5BU5D_t1444911251* L_53 = __this->get_bounds_13();
		int32_t L_54 = __this->get_dir_11();
		NullCheck(L_53);
		int32_t L_55 = L_54;
		float L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_52);
		L_52->set_deathPoint_5(L_56);
		PanelController_t1280165360 * L_57 = V_3;
		NullCheck(L_57);
		L_57->set_duel_7(__this);
		PanelController_t1280165360 * L_58 = V_3;
		__this->set_currentPanel_9(L_58);
		int32_t L_59 = __this->get_dir_11();
		__this->set_dir_11(((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)2)));
		int32_t L_60 = __this->get_dir_11();
		__this->set_dir_11(((int32_t)((int32_t)L_60%(int32_t)4)));
		return;
	}
}
// System.Void DuelController::PlaySafeSound()
extern "C"  void DuelController_PlaySafeSound_m458390668 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		AudioSourceU5BU5D_t4028559421* L_0 = __this->get_safeSounds_6();
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		AudioSourceU5BU5D_t4028559421* L_2 = __this->get_safeSounds_6();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		AudioSource_t3935305588 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		AudioSource_Play_m48294159(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelController::SetPanelAnchor(UnityEngine.GameObject,System.Int32)
extern "C"  void DuelController_SetPanelAnchor_m3823622997 (DuelController_t4245367713 * __this, GameObject_t1113636619 * ___panel0, int32_t ___dir1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController_SetPanelAnchor_m3823622997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___panel0;
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = ___dir1;
		if (L_2)
		{
			goto IL_0051;
		}
	}
	{
		RectTransform_t3704657025 * L_3 = V_0;
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3970636864((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		RectTransform_set_pivot_m909387058(L_3, L_4, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = V_0;
		Vector2_t2156229523  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3970636864((&L_6), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		RectTransform_set_anchorMin_m4230103102(L_5, L_6, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_7 = V_0;
		Vector2_t2156229523  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3970636864((&L_8), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_set_anchorMax_m2998668828(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0051:
	{
		int32_t L_9 = ___dir1;
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0097;
		}
	}
	{
		RectTransform_t3704657025 * L_10 = V_0;
		Vector2_t2156229523  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m3970636864((&L_11), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		RectTransform_set_pivot_m909387058(L_10, L_11, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_12 = V_0;
		Vector2_t2156229523  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m3970636864((&L_13), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		RectTransform_set_anchorMin_m4230103102(L_12, L_13, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = V_0;
		Vector2_t2156229523  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3970636864((&L_15), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchorMax_m2998668828(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0097:
	{
		RectTransform_t3704657025 * L_16 = V_0;
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		RectTransform_set_sizeDelta_m3462269772(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelController::GameOver(System.Int32)
extern "C"  void DuelController_GameOver_m638072373 (DuelController_t4245367713 * __this, int32_t ___winner0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___winner0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0022;
		}
	}
	{
		PanelController_t1280165360 * L_1 = __this->get_currentPanel_9();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_dir_4();
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		___winner0 = 1;
		goto IL_0022;
	}

IL_001f:
	{
		___winner0 = 0;
	}

IL_0022:
	{
		__this->set_playing_12((bool)0);
		AudioSource_t3935305588 * L_3 = __this->get_bgMusic_4();
		NullCheck(L_3);
		AudioSource_Stop_m2682712816(L_3, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_4 = __this->get_failSound_5();
		NullCheck(L_4);
		AudioSource_Play_m48294159(L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_gameOverContainer_2();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		int32_t L_6 = ___winner0;
		if (L_6)
		{
			goto IL_0075;
		}
	}
	{
		GameObject_t1113636619 * L_7 = __this->get_gameOverContainer_2();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (0.0f), (0.0f), (180.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_Rotate_m720511863(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void DuelController::StartOver()
extern "C"  void DuelController_StartOver_m309291912 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelController::ReturnToMenu()
extern "C"  void DuelController_ReturnToMenu_m963487272 (DuelController_t4245367713 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuelController_ReturnToMenu_m963487272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1555075383, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FadeOutController::.ctor()
extern "C"  void FadeOutController__ctor_m1685958263 (FadeOutController_t1417150664 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeOutController::Start()
extern "C"  void FadeOutController_Start_m3423843087 (FadeOutController_t1417150664 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FadeOutController_Start_m3423843087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_i_5(L_0);
		return;
	}
}
// System.Void FadeOutController::StartFading()
extern "C"  void FadeOutController_StartFading_m888056018 (FadeOutController_t1417150664 * __this, const RuntimeMethod* method)
{
	{
		__this->set_fading_2((bool)1);
		return;
	}
}
// System.Void FadeOutController::Update()
extern "C"  void FadeOutController_Update_m2582507079 (FadeOutController_t1417150664 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FadeOutController_Update_m2582507079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Color_t2555686324  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = __this->get_fading_2();
		if (!L_0)
		{
			goto IL_0075;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_fadeSpeed_4();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2));
		Image_t2670269651 * L_3 = __this->get_i_5();
		NullCheck(L_3);
		Color_t2555686324  L_4 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_3);
		V_2 = L_4;
		float L_5 = (&V_2)->get_a_3();
		V_1 = L_5;
		float L_6 = V_1;
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Max_m3146388979(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)L_6, (float)L_7)), (0.0f), /*hidden argument*/NULL);
		V_1 = L_8;
		Image_t2670269651 * L_9 = __this->get_i_5();
		float L_10 = V_1;
		Vector4_t3319028937  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector4__ctor_m2498754347((&L_11), (1.0f), (1.0f), (1.0f), L_10, /*hidden argument*/NULL);
		Color_t2555686324  L_12 = Color_op_Implicit_m2665280078(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_12);
		float L_13 = V_1;
		if ((!(((float)L_13) == ((float)(0.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenuScript::.ctor()
extern "C"  void MainMenuScript__ctor_m2434629044 (MainMenuScript_t2493777029 * __this, const RuntimeMethod* method)
{
	{
		__this->set_padding_4((40.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuScript::Start()
extern "C"  void MainMenuScript_Start_m2530389714 (MainMenuScript_t2493777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuScript_Start_m2530389714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2718991579, 0, /*hidden argument*/NULL);
		__this->set_themeIndex_5(L_0);
		int32_t L_1 = __this->get_themeIndex_5();
		MainMenuScript_SetTheme_m1616100854(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral1410563525, 1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = V_0;
		MainMenuScript_SetUpColorSelectionMenu_m2362659385(__this, L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_colorSelectionContainer_2();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2949325609, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0052:
	{
		GameObject_t1113636619 * L_6 = __this->get_colorSelectionContainer_2();
		NullCheck(L_6);
		GameObject_SetActive_m796801857(L_6, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3717352243, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void MainMenuScript::SetTheme(System.Int32)
extern "C"  void MainMenuScript_SetTheme_m1616100854 (MainMenuScript_t2493777029 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuScript_SetTheme_m1616100854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObjectU5BU5D_t3328599146* V_1 = NULL;
	int32_t V_2 = 0;
	Color_t2555686324  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObjectU5BU5D_t3328599146* V_4 = NULL;
	int32_t V_5 = 0;
	{
		int32_t L_0 = ___index0;
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2718991579, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_1 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_2 = ___index0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_1);
		Color_t2555686324  L_3 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_1)->GetAt(L_2, 0);
		V_0 = L_3;
		GameObjectU5BU5D_t3328599146* L_4 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral1297066703, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_003c;
	}

IL_002a:
	{
		GameObjectU5BU5D_t3328599146* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1113636619 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Image_t2670269651 * L_9 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Color_t2555686324  L_10 = V_0;
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_12 = V_2;
		GameObjectU5BU5D_t3328599146* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_14 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_15 = ___index0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_14);
		Color_t2555686324  L_16 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_14)->GetAt(L_15, 2);
		V_3 = L_16;
		GameObjectU5BU5D_t3328599146* L_17 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral2615087991, /*hidden argument*/NULL);
		V_4 = L_17;
		V_5 = 0;
		goto IL_007c;
	}

IL_0066:
	{
		GameObjectU5BU5D_t3328599146* L_18 = V_4;
		int32_t L_19 = V_5;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		GameObject_t1113636619 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Image_t2670269651 * L_22 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_21, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Color_t2555686324  L_23 = V_3;
		NullCheck(L_22);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_22, L_23);
		int32_t L_24 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_007c:
	{
		int32_t L_25 = V_5;
		GameObjectU5BU5D_t3328599146* L_26 = V_4;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))))))
		{
			goto IL_0066;
		}
	}
	{
		return;
	}
}
// System.Void MainMenuScript::ColorChoiceClicked(UnityEngine.GameObject)
extern "C"  void MainMenuScript_ColorChoiceClicked_m3490894147 (MainMenuScript_t2493777029 * __this, GameObject_t1113636619 * ___go0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MainMenuScript::SetUpColorSelectionMenu(System.Int32)
extern "C"  void MainMenuScript_SetUpColorSelectionMenu_m2362659385 (MainMenuScript_t2493777029 * __this, int32_t ___numThemes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuScript_SetUpColorSelectionMenu_m2362659385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1113636619 * V_1 = NULL;
	float V_2 = 0.0f;
	{
		int32_t L_0 = ___numThemes0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_012e;
	}

IL_0012:
	{
		GameObject_t1113636619 * L_3 = __this->get_colorSelectionPrefab_3();
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_5 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_6 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_1 = L_6;
		GameObject_t1113636619 * L_7 = V_1;
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_colorSelectionContainer_2();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = GameObject_get_transform_m1369836730(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_SetParent_m273471670(L_8, L_10, (bool)0, /*hidden argument*/NULL);
		float L_11 = __this->get_padding_4();
		int32_t L_12 = V_0;
		float L_13 = __this->get_padding_4();
		V_2 = ((float)il2cpp_codegen_add((float)L_11, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_12))), (float)((float)il2cpp_codegen_add((float)(120.0f), (float)L_13))))));
		float L_14 = V_2;
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_17 = V_1;
		NullCheck(L_17);
		RectTransform_t3704657025 * L_18 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_17, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		float L_19 = V_2;
		Vector2_t2156229523  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m3970636864((&L_20), L_19, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		RectTransform_set_anchoredPosition_m4126691837(L_18, L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_21 = V_1;
		NullCheck(L_21);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t3600365921 * L_23 = Transform_Find_m1729760951(L_22, _stringLiteral1403620149, /*hidden argument*/NULL);
		NullCheck(L_23);
		Image_t2670269651 * L_24 = Component_GetComponent_TisImage_t2670269651_m980647750(L_23, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_25 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_26 = V_0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_25);
		Color_t2555686324  L_27 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_25)->GetAt(L_26, 0);
		NullCheck(L_24);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_24, L_27);
		GameObject_t1113636619 * L_28 = V_1;
		NullCheck(L_28);
		Transform_t3600365921 * L_29 = GameObject_get_transform_m1369836730(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t3600365921 * L_30 = Transform_Find_m1729760951(L_29, _stringLiteral2199905863, /*hidden argument*/NULL);
		NullCheck(L_30);
		Image_t2670269651 * L_31 = Component_GetComponent_TisImage_t2670269651_m980647750(L_30, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		ColorU5B0___U2C0___U5D_t941916414* L_32 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_33 = V_0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_32);
		Color_t2555686324  L_34 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_32)->GetAt(L_33, 1);
		NullCheck(L_31);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_31, L_34);
		GameObject_t1113636619 * L_35 = V_1;
		NullCheck(L_35);
		Transform_t3600365921 * L_36 = GameObject_get_transform_m1369836730(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3600365921 * L_37 = Transform_Find_m1729760951(L_36, _stringLiteral3026870195, /*hidden argument*/NULL);
		NullCheck(L_37);
		Image_t2670269651 * L_38 = Component_GetComponent_TisImage_t2670269651_m980647750(L_37, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		ColorU5B0___U2C0___U5D_t941916414* L_39 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_40 = V_0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_39);
		Color_t2555686324  L_41 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_39)->GetAt(L_40, 2);
		NullCheck(L_38);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_38, L_41);
		GameObject_t1113636619 * L_42 = V_1;
		NullCheck(L_42);
		Transform_t3600365921 * L_43 = GameObject_get_transform_m1369836730(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_t3600365921 * L_44 = Transform_Find_m1729760951(L_43, _stringLiteral3659458767, /*hidden argument*/NULL);
		NullCheck(L_44);
		Image_t2670269651 * L_45 = Component_GetComponent_TisImage_t2670269651_m980647750(L_44, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		ColorU5B0___U2C0___U5D_t941916414* L_46 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_47 = V_0;
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_46);
		Color_t2555686324  L_48 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_46)->GetAt(L_47, 3);
		NullCheck(L_45);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_45, L_48);
		GameObject_t1113636619 * L_49 = V_1;
		NullCheck(L_49);
		MenuColorChoiceController_t2734251472 * L_50 = GameObject_GetComponent_TisMenuColorChoiceController_t2734251472_m102361087(L_49, /*hidden argument*/GameObject_GetComponent_TisMenuColorChoiceController_t2734251472_m102361087_RuntimeMethod_var);
		int32_t L_51 = V_0;
		NullCheck(L_50);
		MenuColorChoiceController_SetThemeIndex_m2347622676(L_50, L_51, /*hidden argument*/NULL);
		int32_t L_52 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_012e:
	{
		int32_t L_53 = V_0;
		int32_t L_54 = ___numThemes0;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_0012;
		}
	}
	{
		GameObject_t1113636619 * L_55 = __this->get_colorSelectionContainer_2();
		NullCheck(L_55);
		RectTransform_t3704657025 * L_56 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_55, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		int32_t L_57 = ___numThemes0;
		float L_58 = __this->get_padding_4();
		float L_59 = __this->get_padding_4();
		Vector2_t2156229523  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Vector2__ctor_m3970636864((&L_60), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_57))), (float)((float)il2cpp_codegen_add((float)L_58, (float)(120.0f))))), (float)L_59)), (200.0f), /*hidden argument*/NULL);
		NullCheck(L_56);
		RectTransform_set_sizeDelta_m3462269772(L_56, L_60, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuScript::StartGame()
extern "C"  void MainMenuScript_StartGame_m3950745386 (MainMenuScript_t2493777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuScript_StartGame_m3950745386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral3608622578, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuScript::StartDuel()
extern "C"  void MainMenuScript_StartDuel_m988774669 (MainMenuScript_t2493777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuScript_StartDuel_m988774669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral4287742317, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuColorChoiceController::.ctor()
extern "C"  void MenuColorChoiceController__ctor_m4210969802 (MenuColorChoiceController_t2734251472 * __this, const RuntimeMethod* method)
{
	{
		__this->set_themeIndex_2((-1));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuColorChoiceController::SetThemeIndex(System.Int32)
extern "C"  void MenuColorChoiceController_SetThemeIndex_m2347622676 (MenuColorChoiceController_t2734251472 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		__this->set_themeIndex_2(L_0);
		return;
	}
}
// System.Void MenuColorChoiceController::Clicked()
extern "C"  void MenuColorChoiceController_Clicked_m3268607575 (MenuColorChoiceController_t2734251472 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuColorChoiceController_Clicked_m3268607575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2774581319, /*hidden argument*/NULL);
		NullCheck(L_0);
		MainMenuScript_t2493777029 * L_1 = GameObject_GetComponent_TisMainMenuScript_t2493777029_m3991380284(L_0, /*hidden argument*/GameObject_GetComponent_TisMainMenuScript_t2493777029_m3991380284_RuntimeMethod_var);
		int32_t L_2 = __this->get_themeIndex_2();
		NullCheck(L_1);
		MainMenuScript_SetTheme_m1616100854(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuPanelController::.ctor()
extern "C"  void MenuPanelController__ctor_m3289975908 (MenuPanelController_t3752352500 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_3((400.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuPanelController::Start()
extern "C"  void MenuPanelController_Start_m3547772064 (MenuPanelController_t3752352500 * __this, const RuntimeMethod* method)
{
	{
		__this->set_timer_7((0.0f));
		return;
	}
}
// System.Void MenuPanelController::StartMoving()
extern "C"  void MenuPanelController_StartMoving_m3728774834 (MenuPanelController_t3752352500 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuPanelController_StartMoving_m3728774834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_moving_2((bool)1);
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_rectTransform_5(L_0);
		return;
	}
}
// System.Void MenuPanelController::Update()
extern "C"  void MenuPanelController_Update_m3832367955 (MenuPanelController_t3752352500 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuPanelController_Update_m3832367955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		bool L_0 = __this->get_moving_2();
		if (!L_0)
		{
			goto IL_0077;
		}
	}
	{
		float L_1 = __this->get_speed_3();
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2));
		RectTransform_t3704657025 * L_3 = __this->get_rectTransform_5();
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2183112744(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_1();
		float L_6 = V_0;
		float L_7 = __this->get_maxHeight_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Min_m1073399594(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_5, (float)L_6)), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = V_2;
		Vector2__ctor_m3970636864((&V_1), (0.0f), L_9, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_10 = __this->get_rectTransform_5();
		Vector2_t2156229523  L_11 = V_1;
		NullCheck(L_10);
		RectTransform_set_sizeDelta_m3462269772(L_10, L_11, /*hidden argument*/NULL);
		float L_12 = (&V_1)->get_y_1();
		float L_13 = __this->get_maxHeight_4();
		if ((!(((float)L_12) >= ((float)L_13))))
		{
			goto IL_0072;
		}
	}
	{
		__this->set_moving_2((bool)0);
		__this->set_finalized_8((bool)1);
	}

IL_0072:
	{
		goto IL_0099;
	}

IL_0077:
	{
		bool L_14 = __this->get_finalized_8();
		if (L_14)
		{
			goto IL_0099;
		}
	}
	{
		float L_15 = __this->get_timer_7();
		float L_16 = __this->get_delay_6();
		if ((!(((float)L_15) >= ((float)L_16))))
		{
			goto IL_0099;
		}
	}
	{
		MenuPanelController_StartMoving_m3728774834(__this, /*hidden argument*/NULL);
	}

IL_0099:
	{
		float L_17 = __this->get_timer_7();
		float L_18 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_7(((float)il2cpp_codegen_add((float)L_17, (float)L_18)));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PanelContainerController::.ctor()
extern "C"  void PanelContainerController__ctor_m747984914 (PanelContainerController_t2722594607 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_startScale_3(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->set_endScale_4(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelContainerController::StartShrinking()
extern "C"  void PanelContainerController_StartShrinking_m3156969907 (PanelContainerController_t2722594607 * __this, const RuntimeMethod* method)
{
	{
		__this->set_shrinking_2((bool)1);
		return;
	}
}
// System.Void PanelContainerController::Update()
extern "C"  void PanelContainerController_Update_m836685070 (PanelContainerController_t2722594607 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelContainerController_Update_m836685070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObjectU5BU5D_t3328599146* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = __this->get_shrinking_2();
		if (!L_0)
		{
			goto IL_0046;
		}
	}
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = __this->get_startScale_3();
		Vector3_t3722313464  L_3 = __this->get_endScale_4();
		float L_4 = __this->get_lerpTimer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m3053443106(L_1, L_5, /*hidden argument*/NULL);
		float L_6 = __this->get_lerpTimer_5();
		float L_7 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_shrinkSpeed_6();
		__this->set_lerpTimer_5(((float)il2cpp_codegen_add((float)L_6, (float)((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)))));
	}

IL_0046:
	{
		bool L_9 = __this->get_takingScreenshot_8();
		if (!L_9)
		{
			goto IL_00b2;
		}
	}
	{
		float L_10 = __this->get_screenshotCountdown_7();
		float L_11 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_screenshotCountdown_7(((float)il2cpp_codegen_subtract((float)L_10, (float)L_11)));
		float L_12 = __this->get_screenshotCountdown_7();
		if ((!(((float)L_12) < ((float)(0.0f)))))
		{
			goto IL_00b2;
		}
	}
	{
		__this->set_takingScreenshot_8((bool)0);
		GameObjectU5BU5D_t3328599146* L_13 = __this->get_hiddenForScreenshot_9();
		V_1 = L_13;
		V_2 = 0;
		goto IL_0097;
	}

IL_0088:
	{
		GameObjectU5BU5D_t3328599146* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_t1113636619 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_0 = L_17;
		GameObject_t1113636619 * L_18 = V_0;
		NullCheck(L_18);
		GameObject_SetActive_m796801857(L_18, (bool)1, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0097:
	{
		int32_t L_20 = V_2;
		GameObjectU5BU5D_t3328599146* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_0088;
		}
	}
	{
		__this->set_shrinking_2((bool)1);
		__this->set_lerpTimer_5((0.0f));
	}

IL_00b2:
	{
		return;
	}
}
// System.Void PanelContainerController::TakeScreenshot()
extern "C"  void PanelContainerController_TakeScreenshot_m3670517792 (PanelContainerController_t2722594607 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelContainerController_TakeScreenshot_m3670517792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObjectU5BU5D_t3328599146* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		__this->set_shrinking_2((bool)0);
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = __this->get_startScale_3();
		NullCheck(L_0);
		Transform_set_localScale_m3053443106(L_0, L_1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_2 = __this->get_hiddenForScreenshot_9();
		V_1 = L_2;
		V_2 = 0;
		goto IL_0035;
	}

IL_0026:
	{
		GameObjectU5BU5D_t3328599146* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		GameObject_t1113636619 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = L_6;
		GameObject_t1113636619 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0035:
	{
		int32_t L_9 = V_2;
		GameObjectU5BU5D_t3328599146* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_11 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2841973215, 0, /*hidden argument*/NULL);
		V_3 = L_11;
		int32_t L_12 = V_3;
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral632663995, L_14, _stringLiteral2410385622, /*hidden argument*/NULL);
		ScreenCapture_CaptureScreenshot_m934745658(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2841973215, ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_takingScreenshot_8((bool)1);
		__this->set_screenshotCountdown_7((0.5f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PanelController::.ctor()
extern "C"  void PanelController__ctor_m2725969969 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_3((0.4f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelController::Start()
extern "C"  void PanelController_Start_m564436649 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_Start_m564436649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_moving_2((bool)1);
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_rectTransform_8(L_0);
		float L_1 = __this->get_speed_3();
		int32_t L_2 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_adjustedSpeed_9(((float)il2cpp_codegen_multiply((float)L_1, (float)(((float)((float)L_2))))));
		PanelController_CreateLeadingEdge_m1745492603(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelController::Update()
extern "C"  void PanelController_Update_m3185442511 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_moving_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		PanelController_Move_m1423420284(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void PanelController::CreateLeadingEdge()
extern "C"  void PanelController_CreateLeadingEdge_m1745492603 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_CreateLeadingEdge_m1745492603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2670269651 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_0, _stringLiteral1789606354, /*hidden argument*/NULL);
		__this->set_leadingEdge_11(L_0);
		GameObject_t1113636619 * L_1 = __this->get_leadingEdge_11();
		NullCheck(L_1);
		GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522(L_1, /*hidden argument*/GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522_RuntimeMethod_var);
		GameObject_t1113636619 * L_2 = __this->get_leadingEdge_11();
		NullCheck(L_2);
		Image_t2670269651 * L_3 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_2, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		V_0 = L_3;
		Image_t2670269651 * L_4 = V_0;
		Sprite_t280657092 * L_5 = __this->get_leadingEdgeSprite_10();
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_5, /*hidden argument*/NULL);
		Image_t2670269651 * L_6 = V_0;
		Color_t2555686324  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2943235014((&L_7), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		PanelController_SetLeadingEdgeAnchor_m2694989816(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_leadingEdge_11();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_SetParent_m273471670(L_9, L_10, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PanelController::SetLeadingEdgeAnchor()
extern "C"  void PanelController_SetLeadingEdgeAnchor_m2694989816 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_SetLeadingEdgeAnchor_m2694989816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_leadingEdge_11();
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = __this->get_dir_4();
		if (L_2)
		{
			goto IL_0070;
		}
	}
	{
		RectTransform_t3704657025 * L_3 = V_0;
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3970636864((&L_4), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		RectTransform_set_pivot_m909387058(L_3, L_4, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = V_0;
		Vector2_t2156229523  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3970636864((&L_6), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		RectTransform_set_anchorMin_m4230103102(L_5, L_6, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_7 = V_0;
		Vector2_t2156229523  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3970636864((&L_8), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_set_anchorMax_m2998668828(L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_9 = V_0;
		Vector2_t2156229523  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3970636864((&L_10), (0.0f), (-5.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		RectTransform_set_anchoredPosition_m4126691837(L_9, L_10, /*hidden argument*/NULL);
		goto IL_019a;
	}

IL_0070:
	{
		int32_t L_11 = __this->get_dir_4();
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_00d5;
		}
	}
	{
		RectTransform_t3704657025 * L_12 = V_0;
		Vector2_t2156229523  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m3970636864((&L_13), (1.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_12);
		RectTransform_set_pivot_m909387058(L_12, L_13, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = V_0;
		Vector2_t2156229523  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3970636864((&L_15), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchorMin_m4230103102(L_14, L_15, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_16 = V_0;
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		RectTransform_set_anchorMax_m2998668828(L_16, L_17, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_18 = V_0;
		Vector2_t2156229523  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector2__ctor_m3970636864((&L_19), (5.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		RectTransform_set_anchoredPosition_m4126691837(L_18, L_19, /*hidden argument*/NULL);
		goto IL_019a;
	}

IL_00d5:
	{
		int32_t L_20 = __this->get_dir_4();
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_013a;
		}
	}
	{
		RectTransform_t3704657025 * L_21 = V_0;
		Vector2_t2156229523  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m3970636864((&L_22), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_21);
		RectTransform_set_pivot_m909387058(L_21, L_22, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_23 = V_0;
		Vector2_t2156229523  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3970636864((&L_24), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		RectTransform_set_anchorMin_m4230103102(L_23, L_24, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_25 = V_0;
		Vector2_t2156229523  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector2__ctor_m3970636864((&L_26), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		RectTransform_set_anchorMax_m2998668828(L_25, L_26, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_27 = V_0;
		Vector2_t2156229523  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector2__ctor_m3970636864((&L_28), (0.0f), (5.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m4126691837(L_27, L_28, /*hidden argument*/NULL);
		goto IL_019a;
	}

IL_013a:
	{
		int32_t L_29 = __this->get_dir_4();
		if ((!(((uint32_t)L_29) == ((uint32_t)3))))
		{
			goto IL_019a;
		}
	}
	{
		RectTransform_t3704657025 * L_30 = V_0;
		Vector2_t2156229523  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3970636864((&L_31), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_pivot_m909387058(L_30, L_31, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_32 = V_0;
		Vector2_t2156229523  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_m3970636864((&L_33), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		RectTransform_set_anchorMin_m4230103102(L_32, L_33, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_34 = V_0;
		Vector2_t2156229523  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector2__ctor_m3970636864((&L_35), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_34);
		RectTransform_set_anchorMax_m2998668828(L_34, L_35, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_36 = V_0;
		Vector2_t2156229523  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector2__ctor_m3970636864((&L_37), (-5.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_36);
		RectTransform_set_anchoredPosition_m4126691837(L_36, L_37, /*hidden argument*/NULL);
	}

IL_019a:
	{
		int32_t L_38 = __this->get_dir_4();
		if (((int32_t)((int32_t)L_38%(int32_t)2)))
		{
			goto IL_01c1;
		}
	}
	{
		RectTransform_t3704657025 * L_39 = V_0;
		Vector2_t2156229523  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector2__ctor_m3970636864((&L_40), (0.0f), (5.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		RectTransform_set_sizeDelta_m3462269772(L_39, L_40, /*hidden argument*/NULL);
		goto IL_01d6;
	}

IL_01c1:
	{
		RectTransform_t3704657025 * L_41 = V_0;
		Vector2_t2156229523  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector2__ctor_m3970636864((&L_42), (5.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		RectTransform_set_sizeDelta_m3462269772(L_41, L_42, /*hidden argument*/NULL);
	}

IL_01d6:
	{
		return;
	}
}
// System.Void PanelController::Move()
extern "C"  void PanelController_Move_m1423420284 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_Move_m1423420284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_adjustedSpeed_9();
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_multiply((float)L_0, (float)L_1));
		int32_t L_2 = __this->get_dir_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = __this->get_dir_4();
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_006b;
		}
	}

IL_0024:
	{
		RectTransform_t3704657025 * L_4 = __this->get_rectTransform_8();
		NullCheck(L_4);
		Vector2_t2156229523  L_5 = RectTransform_get_sizeDelta_m2183112744(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t2156229523  L_6 = V_1;
		float L_7 = V_0;
		Vector2_t2156229523  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3970636864((&L_8), (0.0f), L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_9 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		RectTransform_t3704657025 * L_10 = __this->get_rectTransform_8();
		Vector2_t2156229523  L_11 = V_1;
		NullCheck(L_10);
		RectTransform_set_sizeDelta_m3462269772(L_10, L_11, /*hidden argument*/NULL);
		float L_12 = (&V_1)->get_y_1();
		float L_13 = __this->get_deathPoint_5();
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0066;
		}
	}
	{
		PanelController_GameOver_m2942013208(__this, /*hidden argument*/NULL);
	}

IL_0066:
	{
		goto IL_00ad;
	}

IL_006b:
	{
		RectTransform_t3704657025 * L_14 = __this->get_rectTransform_8();
		NullCheck(L_14);
		Vector2_t2156229523  L_15 = RectTransform_get_sizeDelta_m2183112744(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Vector2_t2156229523  L_16 = V_2;
		float L_17 = V_0;
		Vector2_t2156229523  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3970636864((&L_18), L_17, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_19 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		RectTransform_t3704657025 * L_20 = __this->get_rectTransform_8();
		Vector2_t2156229523  L_21 = V_2;
		NullCheck(L_20);
		RectTransform_set_sizeDelta_m3462269772(L_20, L_21, /*hidden argument*/NULL);
		float L_22 = (&V_2)->get_x_0();
		float L_23 = __this->get_deathPoint_5();
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_00ad;
		}
	}
	{
		PanelController_GameOver_m2942013208(__this, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		return;
	}
}
// System.Single PanelController::StopMoving()
extern "C"  float PanelController_StopMoving_m198065155 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_StopMoving_m198065155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1113636619 * L_0 = __this->get_leadingEdge_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_moving_2((bool)0);
		int32_t L_1 = __this->get_dir_4();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = __this->get_dir_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_003d;
		}
	}

IL_0029:
	{
		RectTransform_t3704657025 * L_3 = __this->get_rectTransform_8();
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2183112744(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_y_1();
		return L_5;
	}

IL_003d:
	{
		RectTransform_t3704657025 * L_6 = __this->get_rectTransform_8();
		NullCheck(L_6);
		Vector2_t2156229523  L_7 = RectTransform_get_sizeDelta_m2183112744(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_0();
		return L_8;
	}
}
// System.Void PanelController::GameOver()
extern "C"  void PanelController_GameOver_m2942013208 (PanelController_t1280165360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PanelController_GameOver_m2942013208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PanelController_StopMoving_m198065155(__this, /*hidden argument*/NULL);
		SquareController_t2553685805 * L_0 = __this->get_square_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		SquareController_t2553685805 * L_2 = __this->get_square_6();
		NullCheck(L_2);
		SquareController_GameOver_m1811381354(L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		DuelController_t4245367713 * L_3 = __this->get_duel_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		DuelController_t4245367713 * L_5 = __this->get_duel_7();
		NullCheck(L_5);
		DuelController_GameOver_m638072373(L_5, (-1), /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SquareController::.ctor()
extern "C"  void SquareController__ctor_m2819238152 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController__ctor_m2819238152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_playing_23((bool)1);
		__this->set_bounds_24(((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::Start()
extern "C"  void SquareController_Start_m688405192 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_Start_m688405192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral1410563525, 1, /*hidden argument*/NULL);
		__this->set_numThemesUnlocked_25(L_0);
		int32_t L_1 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2718991579, 0, /*hidden argument*/NULL);
		__this->set_colorScheme_20(L_1);
		SingleU5BU5D_t1444911251* L_2 = __this->get_bounds_24();
		int32_t L_3 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(((float)((float)L_3))));
		SingleU5BU5D_t1444911251* L_4 = __this->get_bounds_24();
		int32_t L_5 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)(((float)((float)L_5))));
		SingleU5BU5D_t1444911251* L_6 = __this->get_bounds_24();
		int32_t L_7 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)(((float)((float)L_7))));
		SingleU5BU5D_t1444911251* L_8 = __this->get_bounds_24();
		int32_t L_9 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)(((float)((float)L_9))));
		int32_t L_10 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2574543678, 0, /*hidden argument*/NULL);
		__this->set_highScore_2(L_10);
		return;
	}
}
// System.Void SquareController::Update()
extern "C"  void SquareController_Update_m1451841740 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_Update_m1451841740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_playing_23();
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		PanelController_t1280165360 * L_2 = __this->get_currentPanel_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		SquareController_PlaySafeSound_m273964245(__this, /*hidden argument*/NULL);
		SquareController_IncreaseScore_m3643553335(__this, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_score_22();
		if (((int32_t)((int32_t)L_4%(int32_t)5)))
		{
			goto IL_0045;
		}
	}
	{
		SquareController_PlayAccentSound_m1646413189(__this, /*hidden argument*/NULL);
	}

IL_0045:
	{
		SquareController_CreatePanel_m2690008112(__this, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void SquareController::CreatePanel()
extern "C"  void SquareController_CreatePanel_m2690008112 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_CreatePanel_m2690008112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	GameObject_t1113636619 * V_1 = NULL;
	Image_t2670269651 * V_2 = NULL;
	PanelController_t1280165360 * V_3 = NULL;
	{
		PanelController_t1280165360 * L_0 = __this->get_currentPanel_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0106;
		}
	}
	{
		PanelController_t1280165360 * L_2 = __this->get_currentPanel_19();
		NullCheck(L_2);
		float L_3 = PanelController_StopMoving_m198065155(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PanelController_t1280165360 * L_4 = __this->get_currentPanel_19();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_dir_4();
		if (L_5)
		{
			goto IL_0057;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_6 = __this->get_bounds_24();
		float L_7 = V_0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_7);
		SingleU5BU5D_t1444911251* L_8 = __this->get_bounds_24();
		SingleU5BU5D_t1444911251* L_9 = __this->get_bounds_24();
		NullCheck(L_9);
		int32_t L_10 = 2;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_14 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_11, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_12))), (float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_14);
		goto IL_0106;
	}

IL_0057:
	{
		PanelController_t1280165360 * L_15 = __this->get_currentPanel_19();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_dir_4();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0093;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_17 = __this->get_bounds_24();
		float L_18 = V_0;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_18);
		SingleU5BU5D_t1444911251* L_19 = __this->get_bounds_24();
		SingleU5BU5D_t1444911251* L_20 = __this->get_bounds_24();
		NullCheck(L_20);
		int32_t L_21 = 3;
		float L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_22, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_23))), (float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_25);
		goto IL_0106;
	}

IL_0093:
	{
		PanelController_t1280165360 * L_26 = __this->get_currentPanel_19();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_dir_4();
		if ((!(((uint32_t)L_27) == ((uint32_t)2))))
		{
			goto IL_00cf;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_28 = __this->get_bounds_24();
		float L_29 = V_0;
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_29);
		SingleU5BU5D_t1444911251* L_30 = __this->get_bounds_24();
		SingleU5BU5D_t1444911251* L_31 = __this->get_bounds_24();
		NullCheck(L_31);
		int32_t L_32 = 0;
		float L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		int32_t L_34 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_35 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_33, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_34))), (float)L_35)), /*hidden argument*/NULL);
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_36);
		goto IL_0106;
	}

IL_00cf:
	{
		PanelController_t1280165360 * L_37 = __this->get_currentPanel_19();
		NullCheck(L_37);
		int32_t L_38 = L_37->get_dir_4();
		if ((!(((uint32_t)L_38) == ((uint32_t)3))))
		{
			goto IL_0106;
		}
	}
	{
		SingleU5BU5D_t1444911251* L_39 = __this->get_bounds_24();
		float L_40 = V_0;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_40);
		SingleU5BU5D_t1444911251* L_41 = __this->get_bounds_24();
		SingleU5BU5D_t1444911251* L_42 = __this->get_bounds_24();
		NullCheck(L_42);
		int32_t L_43 = 1;
		float L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		int32_t L_45 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_47 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_44, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_45))), (float)L_46)), /*hidden argument*/NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_47);
	}

IL_0106:
	{
		GameObject_t1113636619 * L_48 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_48, _stringLiteral1597722606, /*hidden argument*/NULL);
		V_1 = L_48;
		GameObject_t1113636619 * L_49 = V_1;
		NullCheck(L_49);
		GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522(L_49, /*hidden argument*/GameObject_AddComponent_TisCanvasRenderer_t2598313366_m463196522_RuntimeMethod_var);
		GameObject_t1113636619 * L_50 = V_1;
		NullCheck(L_50);
		Image_t2670269651 * L_51 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_50, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		V_2 = L_51;
		Image_t2670269651 * L_52 = V_2;
		Sprite_t280657092 * L_53 = __this->get_panelSprite_18();
		NullCheck(L_52);
		Image_set_sprite_m2369174689(L_52, L_53, /*hidden argument*/NULL);
		Image_t2670269651 * L_54 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_55 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		int32_t L_56 = __this->get_colorScheme_20();
		int32_t L_57 = __this->get_dir_21();
		NullCheck((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_55);
		Color_t2555686324  L_58 = ((ColorU5B0___U2C0___U5D_t941916414*)(ColorU5B0___U2C0___U5D_t941916414*)L_55)->GetAt(L_56, L_57);
		NullCheck(L_54);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_54, L_58);
		GameObject_t1113636619 * L_59 = V_1;
		int32_t L_60 = __this->get_dir_21();
		SquareController_SetPanelAnchor_m2372856663(__this, L_59, L_60, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_61 = V_1;
		NullCheck(L_61);
		Transform_t3600365921 * L_62 = GameObject_get_transform_m1369836730(L_61, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_63 = __this->get_allPanelsContainer_17();
		NullCheck(L_63);
		Transform_t3600365921 * L_64 = GameObject_get_transform_m1369836730(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		Transform_SetParent_m273471670(L_62, L_64, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_65 = V_1;
		RuntimeTypeHandle_t3027515415  L_66 = { reinterpret_cast<intptr_t> (PanelController_t1280165360_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_67 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		NullCheck(L_65);
		Component_t1923634451 * L_68 = GameObject_AddComponent_m136524825(L_65, L_67, /*hidden argument*/NULL);
		V_3 = ((PanelController_t1280165360 *)IsInstClass((RuntimeObject*)L_68, PanelController_t1280165360_il2cpp_TypeInfo_var));
		PanelController_t1280165360 * L_69 = V_3;
		Sprite_t280657092 * L_70 = __this->get_panelSprite_18();
		NullCheck(L_69);
		L_69->set_leadingEdgeSprite_10(L_70);
		PanelController_t1280165360 * L_71 = V_3;
		int32_t L_72 = __this->get_dir_21();
		NullCheck(L_71);
		L_71->set_dir_4(L_72);
		PanelController_t1280165360 * L_73 = V_3;
		SingleU5BU5D_t1444911251* L_74 = __this->get_bounds_24();
		int32_t L_75 = __this->get_dir_21();
		NullCheck(L_74);
		int32_t L_76 = L_75;
		float L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		NullCheck(L_73);
		L_73->set_deathPoint_5(L_77);
		PanelController_t1280165360 * L_78 = V_3;
		NullCheck(L_78);
		L_78->set_square_6(__this);
		PanelController_t1280165360 * L_79 = V_3;
		__this->set_currentPanel_19(L_79);
		int32_t L_80 = __this->get_dir_21();
		__this->set_dir_21(((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1)));
		int32_t L_81 = __this->get_dir_21();
		__this->set_dir_21(((int32_t)((int32_t)L_81%(int32_t)4)));
		return;
	}
}
// System.Void SquareController::PlaySafeSound()
extern "C"  void SquareController_PlaySafeSound_m273964245 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		AudioSourceU5BU5D_t4028559421* L_0 = __this->get_safeSounds_15();
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		AudioSourceU5BU5D_t4028559421* L_2 = __this->get_safeSounds_15();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		AudioSource_t3935305588 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		AudioSource_Play_m48294159(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::PlayAccentSound()
extern "C"  void SquareController_PlayAccentSound_m1646413189 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		AudioSourceU5BU5D_t4028559421* L_0 = __this->get_accentSounds_13();
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		AudioSourceU5BU5D_t4028559421* L_2 = __this->get_accentSounds_13();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		AudioSource_t3935305588 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		AudioSource_Play_m48294159(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::IncreaseScore()
extern "C"  void SquareController_IncreaseScore_m3643553335 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_IncreaseScore_m3643553335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_score_22();
		__this->set_score_22(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		Text_t1901882714 * L_1 = __this->get_scoreText_4();
		int32_t* L_2 = __this->get_address_of_score_22();
		String_t* L_3 = Int32_ToString_m141394615(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		int32_t L_4 = __this->get_score_22();
		int32_t L_5 = __this->get_highScore_2();
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_6 = __this->get_score_22();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2574543678, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_setHighScore_3();
		if (L_7)
		{
			goto IL_0063;
		}
	}
	{
		__this->set_setHighScore_3((bool)1);
		SquareController_Applaud_m2287170021(__this, /*hidden argument*/NULL);
	}

IL_0063:
	{
		bool L_8 = __this->get_unlockedATheme_26();
		if (L_8)
		{
			goto IL_0156;
		}
	}
	{
		int32_t L_9 = __this->get_score_22();
		if ((((int32_t)L_9) < ((int32_t)((int32_t)10))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_10 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_0150;
		}
	}

IL_0087:
	{
		int32_t L_11 = __this->get_score_22();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)15))))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_12 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_12) == ((int32_t)2)))
		{
			goto IL_0150;
		}
	}

IL_00a0:
	{
		int32_t L_13 = __this->get_score_22();
		if ((((int32_t)L_13) < ((int32_t)((int32_t)20))))
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_14 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_14) == ((int32_t)3)))
		{
			goto IL_0150;
		}
	}

IL_00b9:
	{
		int32_t L_15 = __this->get_score_22();
		if ((((int32_t)L_15) < ((int32_t)((int32_t)25))))
		{
			goto IL_00d2;
		}
	}
	{
		int32_t L_16 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_16) == ((int32_t)4)))
		{
			goto IL_0150;
		}
	}

IL_00d2:
	{
		int32_t L_17 = __this->get_score_22();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)30))))
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_18 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_18) == ((int32_t)5)))
		{
			goto IL_0150;
		}
	}

IL_00eb:
	{
		int32_t L_19 = __this->get_score_22();
		if ((((int32_t)L_19) < ((int32_t)((int32_t)35))))
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_20 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_20) == ((int32_t)6)))
		{
			goto IL_0150;
		}
	}

IL_0104:
	{
		int32_t L_21 = __this->get_score_22();
		if ((((int32_t)L_21) < ((int32_t)((int32_t)40))))
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_22 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_22) == ((int32_t)7)))
		{
			goto IL_0150;
		}
	}

IL_011d:
	{
		int32_t L_23 = __this->get_score_22();
		if ((((int32_t)L_23) < ((int32_t)((int32_t)45))))
		{
			goto IL_0136;
		}
	}
	{
		int32_t L_24 = __this->get_numThemesUnlocked_25();
		if ((((int32_t)L_24) == ((int32_t)8)))
		{
			goto IL_0150;
		}
	}

IL_0136:
	{
		int32_t L_25 = __this->get_score_22();
		if ((((int32_t)L_25) < ((int32_t)((int32_t)50))))
		{
			goto IL_0156;
		}
	}
	{
		int32_t L_26 = __this->get_numThemesUnlocked_25();
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0156;
		}
	}

IL_0150:
	{
		SquareController_UnlockTheme_m880230570(__this, /*hidden argument*/NULL);
	}

IL_0156:
	{
		return;
	}
}
// System.Void SquareController::UnlockTheme()
extern "C"  void SquareController_UnlockTheme_m880230570 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_UnlockTheme_m880230570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_numThemesUnlocked_25();
		__this->set_numThemesUnlocked_25(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		__this->set_unlockedATheme_26((bool)1);
		int32_t L_1 = __this->get_numThemesUnlocked_25();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral1410563525, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::Applaud()
extern "C"  void SquareController_Applaud_m2287170021 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	{
		AudioSource_t3935305588 * L_0 = __this->get_applause_12();
		NullCheck(L_0);
		AudioSource_Play_m48294159(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::SetPanelAnchor(UnityEngine.GameObject,System.Int32)
extern "C"  void SquareController_SetPanelAnchor_m2372856663 (SquareController_t2553685805 * __this, GameObject_t1113636619 * ___panel0, int32_t ___dir1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_SetPanelAnchor_m2372856663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___panel0;
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = ___dir1;
		if (L_2)
		{
			goto IL_0051;
		}
	}
	{
		RectTransform_t3704657025 * L_3 = V_0;
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3970636864((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		RectTransform_set_pivot_m909387058(L_3, L_4, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = V_0;
		Vector2_t2156229523  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3970636864((&L_6), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		RectTransform_set_anchorMin_m4230103102(L_5, L_6, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_7 = V_0;
		Vector2_t2156229523  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3970636864((&L_8), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_set_anchorMax_m2998668828(L_7, L_8, /*hidden argument*/NULL);
		goto IL_012d;
	}

IL_0051:
	{
		int32_t L_9 = ___dir1;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_009c;
		}
	}
	{
		RectTransform_t3704657025 * L_10 = V_0;
		Vector2_t2156229523  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m3970636864((&L_11), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_10);
		RectTransform_set_pivot_m909387058(L_10, L_11, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_12 = V_0;
		Vector2_t2156229523  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m3970636864((&L_13), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		RectTransform_set_anchorMin_m4230103102(L_12, L_13, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = V_0;
		Vector2_t2156229523  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3970636864((&L_15), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchorMax_m2998668828(L_14, L_15, /*hidden argument*/NULL);
		goto IL_012d;
	}

IL_009c:
	{
		int32_t L_16 = ___dir1;
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_00e7;
		}
	}
	{
		RectTransform_t3704657025 * L_17 = V_0;
		Vector2_t2156229523  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3970636864((&L_18), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_pivot_m909387058(L_17, L_18, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_19 = V_0;
		Vector2_t2156229523  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m3970636864((&L_20), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		RectTransform_set_anchorMin_m4230103102(L_19, L_20, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_21 = V_0;
		Vector2_t2156229523  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m3970636864((&L_22), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_21);
		RectTransform_set_anchorMax_m2998668828(L_21, L_22, /*hidden argument*/NULL);
		goto IL_012d;
	}

IL_00e7:
	{
		int32_t L_23 = ___dir1;
		if ((!(((uint32_t)L_23) == ((uint32_t)3))))
		{
			goto IL_012d;
		}
	}
	{
		RectTransform_t3704657025 * L_24 = V_0;
		Vector2_t2156229523  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m3970636864((&L_25), (1.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_pivot_m909387058(L_24, L_25, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_26 = V_0;
		Vector2_t2156229523  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector2__ctor_m3970636864((&L_27), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4230103102(L_26, L_27, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_28 = V_0;
		Vector2_t2156229523  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector2__ctor_m3970636864((&L_29), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		RectTransform_set_anchorMax_m2998668828(L_28, L_29, /*hidden argument*/NULL);
	}

IL_012d:
	{
		RectTransform_t3704657025 * L_30 = V_0;
		Vector2_t2156229523  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3970636864((&L_31), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_sizeDelta_m3462269772(L_30, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::GameOver()
extern "C"  void SquareController_GameOver_m1811381354 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_GameOver_m1811381354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_playing_23((bool)0);
		AudioSource_t3935305588 * L_0 = __this->get_bgMusic_10();
		NullCheck(L_0);
		AudioSource_Stop_m2682712816(L_0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_1 = __this->get_failSound_14();
		NullCheck(L_1);
		AudioSource_Play_m48294159(L_1, /*hidden argument*/NULL);
		Text_t1901882714 * L_2 = __this->get_scoreText_4();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_exportButton_9();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_panelContainer_16();
		NullCheck(L_5);
		PanelContainerController_t2722594607 * L_6 = GameObject_GetComponent_TisPanelContainerController_t2722594607_m3234714783(L_5, /*hidden argument*/GameObject_GetComponent_TisPanelContainerController_t2722594607_m3234714783_RuntimeMethod_var);
		NullCheck(L_6);
		PanelContainerController_StartShrinking_m3156969907(L_6, /*hidden argument*/NULL);
		SquareController_UpdateGameOverGUI_m959589948(__this, /*hidden argument*/NULL);
		FadeOutController_t1417150664 * L_7 = __this->get_gameOverFade_27();
		NullCheck(L_7);
		FadeOutController_StartFading_m888056018(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::UpdateGameOverGUI()
extern "C"  void SquareController_UpdateGameOverGUI_m959589948 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_UpdateGameOverGUI_m959589948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_gameOverContainer_5();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_1 = __this->get_gameOverScoreText_6();
		int32_t* L_2 = __this->get_address_of_score_22();
		String_t* L_3 = Int32_ToString_m141394615(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		bool L_4 = __this->get_setHighScore_3();
		if (!L_4)
		{
			goto IL_0090;
		}
	}
	{
		Text_t1901882714 * L_5 = __this->get_gameOverRecordText_7();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral2673082146);
		Text_t1901882714 * L_6 = __this->get_gameOverRecordText_7();
		Color_t2555686324  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2943235014((&L_7), (0.12f), (0.88f), (0.69f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		Text_t1901882714 * L_8 = __this->get_gameOverScoreText_6();
		Color_t2555686324  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m2943235014((&L_9), (0.12f), (0.88f), (0.69f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_9);
		goto IL_00b0;
	}

IL_0090:
	{
		Text_t1901882714 * L_10 = __this->get_gameOverRecordText_7();
		int32_t L_11 = __this->get_highScore_2();
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral811457787, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_14);
	}

IL_00b0:
	{
		bool L_15 = __this->get_unlockedATheme_26();
		if (!L_15)
		{
			goto IL_00f6;
		}
	}
	{
		int32_t L_16 = __this->get_numThemesUnlocked_25();
		IL2CPP_RUNTIME_CLASS_INIT(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var);
		ColorU5B0___U2C0___U5D_t941916414* L_17 = ((ColorSchemeManager_t1974266384_StaticFields*)il2cpp_codegen_static_fields_for(ColorSchemeManager_t1974266384_il2cpp_TypeInfo_var))->get_colorSchemes_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_17);
		int32_t L_18 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_17, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_18))))
		{
			goto IL_00e6;
		}
	}
	{
		Text_t1901882714 * L_19 = __this->get_newThemeText_8();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, _stringLiteral1390561909);
		goto IL_00f6;
	}

IL_00e6:
	{
		Text_t1901882714 * L_20 = __this->get_newThemeText_8();
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, _stringLiteral2460025446);
	}

IL_00f6:
	{
		return;
	}
}
// System.Void SquareController::StartOver()
extern "C"  void SquareController_StartOver_m3934145598 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SquareController::ReturnToMenu()
extern "C"  void SquareController_ReturnToMenu_m1730121986 (SquareController_t2553685805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareController_ReturnToMenu_m1730121986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1555075383, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
