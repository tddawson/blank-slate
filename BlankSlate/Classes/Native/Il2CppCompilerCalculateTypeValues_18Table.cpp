﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t941916414;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// SquareController
struct SquareController_t2553685805;
// DuelController
struct DuelController_t4245367713;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t4028559421;
// PanelController
struct PanelController_t1280165360;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// FadeOutController
struct FadeOutController_t1417150664;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef COLORSCHEMEMANAGER_T1974266384_H
#define COLORSCHEMEMANAGER_T1974266384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSchemeManager
struct  ColorSchemeManager_t1974266384  : public RuntimeObject
{
public:

public:
};

struct ColorSchemeManager_t1974266384_StaticFields
{
public:
	// UnityEngine.Color[0...,0...] ColorSchemeManager::colorSchemes
	ColorU5B0___U2C0___U5D_t941916414* ___colorSchemes_0;

public:
	inline static int32_t get_offset_of_colorSchemes_0() { return static_cast<int32_t>(offsetof(ColorSchemeManager_t1974266384_StaticFields, ___colorSchemes_0)); }
	inline ColorU5B0___U2C0___U5D_t941916414* get_colorSchemes_0() const { return ___colorSchemes_0; }
	inline ColorU5B0___U2C0___U5D_t941916414** get_address_of_colorSchemes_0() { return &___colorSchemes_0; }
	inline void set_colorSchemes_0(ColorU5B0___U2C0___U5D_t941916414* value)
	{
		___colorSchemes_0 = value;
		Il2CppCodeGenWriteBarrier((&___colorSchemes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSCHEMEMANAGER_T1974266384_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PANELCONTROLLER_T1280165360_H
#define PANELCONTROLLER_T1280165360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelController
struct  PanelController_t1280165360  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PanelController::moving
	bool ___moving_2;
	// System.Single PanelController::speed
	float ___speed_3;
	// System.Int32 PanelController::dir
	int32_t ___dir_4;
	// System.Single PanelController::deathPoint
	float ___deathPoint_5;
	// SquareController PanelController::square
	SquareController_t2553685805 * ___square_6;
	// DuelController PanelController::duel
	DuelController_t4245367713 * ___duel_7;
	// UnityEngine.RectTransform PanelController::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// System.Single PanelController::adjustedSpeed
	float ___adjustedSpeed_9;
	// UnityEngine.Sprite PanelController::leadingEdgeSprite
	Sprite_t280657092 * ___leadingEdgeSprite_10;
	// UnityEngine.GameObject PanelController::leadingEdge
	GameObject_t1113636619 * ___leadingEdge_11;

public:
	inline static int32_t get_offset_of_moving_2() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___moving_2)); }
	inline bool get_moving_2() const { return ___moving_2; }
	inline bool* get_address_of_moving_2() { return &___moving_2; }
	inline void set_moving_2(bool value)
	{
		___moving_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_dir_4() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___dir_4)); }
	inline int32_t get_dir_4() const { return ___dir_4; }
	inline int32_t* get_address_of_dir_4() { return &___dir_4; }
	inline void set_dir_4(int32_t value)
	{
		___dir_4 = value;
	}

	inline static int32_t get_offset_of_deathPoint_5() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___deathPoint_5)); }
	inline float get_deathPoint_5() const { return ___deathPoint_5; }
	inline float* get_address_of_deathPoint_5() { return &___deathPoint_5; }
	inline void set_deathPoint_5(float value)
	{
		___deathPoint_5 = value;
	}

	inline static int32_t get_offset_of_square_6() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___square_6)); }
	inline SquareController_t2553685805 * get_square_6() const { return ___square_6; }
	inline SquareController_t2553685805 ** get_address_of_square_6() { return &___square_6; }
	inline void set_square_6(SquareController_t2553685805 * value)
	{
		___square_6 = value;
		Il2CppCodeGenWriteBarrier((&___square_6), value);
	}

	inline static int32_t get_offset_of_duel_7() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___duel_7)); }
	inline DuelController_t4245367713 * get_duel_7() const { return ___duel_7; }
	inline DuelController_t4245367713 ** get_address_of_duel_7() { return &___duel_7; }
	inline void set_duel_7(DuelController_t4245367713 * value)
	{
		___duel_7 = value;
		Il2CppCodeGenWriteBarrier((&___duel_7), value);
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_adjustedSpeed_9() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___adjustedSpeed_9)); }
	inline float get_adjustedSpeed_9() const { return ___adjustedSpeed_9; }
	inline float* get_address_of_adjustedSpeed_9() { return &___adjustedSpeed_9; }
	inline void set_adjustedSpeed_9(float value)
	{
		___adjustedSpeed_9 = value;
	}

	inline static int32_t get_offset_of_leadingEdgeSprite_10() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___leadingEdgeSprite_10)); }
	inline Sprite_t280657092 * get_leadingEdgeSprite_10() const { return ___leadingEdgeSprite_10; }
	inline Sprite_t280657092 ** get_address_of_leadingEdgeSprite_10() { return &___leadingEdgeSprite_10; }
	inline void set_leadingEdgeSprite_10(Sprite_t280657092 * value)
	{
		___leadingEdgeSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___leadingEdgeSprite_10), value);
	}

	inline static int32_t get_offset_of_leadingEdge_11() { return static_cast<int32_t>(offsetof(PanelController_t1280165360, ___leadingEdge_11)); }
	inline GameObject_t1113636619 * get_leadingEdge_11() const { return ___leadingEdge_11; }
	inline GameObject_t1113636619 ** get_address_of_leadingEdge_11() { return &___leadingEdge_11; }
	inline void set_leadingEdge_11(GameObject_t1113636619 * value)
	{
		___leadingEdge_11 = value;
		Il2CppCodeGenWriteBarrier((&___leadingEdge_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELCONTROLLER_T1280165360_H
#ifndef PANELCONTAINERCONTROLLER_T2722594607_H
#define PANELCONTAINERCONTROLLER_T2722594607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelContainerController
struct  PanelContainerController_t2722594607  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PanelContainerController::shrinking
	bool ___shrinking_2;
	// UnityEngine.Vector3 PanelContainerController::startScale
	Vector3_t3722313464  ___startScale_3;
	// UnityEngine.Vector3 PanelContainerController::endScale
	Vector3_t3722313464  ___endScale_4;
	// System.Single PanelContainerController::lerpTimer
	float ___lerpTimer_5;
	// System.Single PanelContainerController::shrinkSpeed
	float ___shrinkSpeed_6;
	// System.Single PanelContainerController::screenshotCountdown
	float ___screenshotCountdown_7;
	// System.Boolean PanelContainerController::takingScreenshot
	bool ___takingScreenshot_8;
	// UnityEngine.GameObject[] PanelContainerController::hiddenForScreenshot
	GameObjectU5BU5D_t3328599146* ___hiddenForScreenshot_9;

public:
	inline static int32_t get_offset_of_shrinking_2() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___shrinking_2)); }
	inline bool get_shrinking_2() const { return ___shrinking_2; }
	inline bool* get_address_of_shrinking_2() { return &___shrinking_2; }
	inline void set_shrinking_2(bool value)
	{
		___shrinking_2 = value;
	}

	inline static int32_t get_offset_of_startScale_3() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___startScale_3)); }
	inline Vector3_t3722313464  get_startScale_3() const { return ___startScale_3; }
	inline Vector3_t3722313464 * get_address_of_startScale_3() { return &___startScale_3; }
	inline void set_startScale_3(Vector3_t3722313464  value)
	{
		___startScale_3 = value;
	}

	inline static int32_t get_offset_of_endScale_4() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___endScale_4)); }
	inline Vector3_t3722313464  get_endScale_4() const { return ___endScale_4; }
	inline Vector3_t3722313464 * get_address_of_endScale_4() { return &___endScale_4; }
	inline void set_endScale_4(Vector3_t3722313464  value)
	{
		___endScale_4 = value;
	}

	inline static int32_t get_offset_of_lerpTimer_5() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___lerpTimer_5)); }
	inline float get_lerpTimer_5() const { return ___lerpTimer_5; }
	inline float* get_address_of_lerpTimer_5() { return &___lerpTimer_5; }
	inline void set_lerpTimer_5(float value)
	{
		___lerpTimer_5 = value;
	}

	inline static int32_t get_offset_of_shrinkSpeed_6() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___shrinkSpeed_6)); }
	inline float get_shrinkSpeed_6() const { return ___shrinkSpeed_6; }
	inline float* get_address_of_shrinkSpeed_6() { return &___shrinkSpeed_6; }
	inline void set_shrinkSpeed_6(float value)
	{
		___shrinkSpeed_6 = value;
	}

	inline static int32_t get_offset_of_screenshotCountdown_7() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___screenshotCountdown_7)); }
	inline float get_screenshotCountdown_7() const { return ___screenshotCountdown_7; }
	inline float* get_address_of_screenshotCountdown_7() { return &___screenshotCountdown_7; }
	inline void set_screenshotCountdown_7(float value)
	{
		___screenshotCountdown_7 = value;
	}

	inline static int32_t get_offset_of_takingScreenshot_8() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___takingScreenshot_8)); }
	inline bool get_takingScreenshot_8() const { return ___takingScreenshot_8; }
	inline bool* get_address_of_takingScreenshot_8() { return &___takingScreenshot_8; }
	inline void set_takingScreenshot_8(bool value)
	{
		___takingScreenshot_8 = value;
	}

	inline static int32_t get_offset_of_hiddenForScreenshot_9() { return static_cast<int32_t>(offsetof(PanelContainerController_t2722594607, ___hiddenForScreenshot_9)); }
	inline GameObjectU5BU5D_t3328599146* get_hiddenForScreenshot_9() const { return ___hiddenForScreenshot_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_hiddenForScreenshot_9() { return &___hiddenForScreenshot_9; }
	inline void set_hiddenForScreenshot_9(GameObjectU5BU5D_t3328599146* value)
	{
		___hiddenForScreenshot_9 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenForScreenshot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANELCONTAINERCONTROLLER_T2722594607_H
#ifndef MENUPANELCONTROLLER_T3752352500_H
#define MENUPANELCONTROLLER_T3752352500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuPanelController
struct  MenuPanelController_t3752352500  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MenuPanelController::moving
	bool ___moving_2;
	// System.Single MenuPanelController::speed
	float ___speed_3;
	// System.Single MenuPanelController::maxHeight
	float ___maxHeight_4;
	// UnityEngine.RectTransform MenuPanelController::rectTransform
	RectTransform_t3704657025 * ___rectTransform_5;
	// System.Single MenuPanelController::delay
	float ___delay_6;
	// System.Single MenuPanelController::timer
	float ___timer_7;
	// System.Boolean MenuPanelController::finalized
	bool ___finalized_8;

public:
	inline static int32_t get_offset_of_moving_2() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___moving_2)); }
	inline bool get_moving_2() const { return ___moving_2; }
	inline bool* get_address_of_moving_2() { return &___moving_2; }
	inline void set_moving_2(bool value)
	{
		___moving_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_maxHeight_4() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___maxHeight_4)); }
	inline float get_maxHeight_4() const { return ___maxHeight_4; }
	inline float* get_address_of_maxHeight_4() { return &___maxHeight_4; }
	inline void set_maxHeight_4(float value)
	{
		___maxHeight_4 = value;
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___rectTransform_5)); }
	inline RectTransform_t3704657025 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t3704657025 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}

	inline static int32_t get_offset_of_delay_6() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___delay_6)); }
	inline float get_delay_6() const { return ___delay_6; }
	inline float* get_address_of_delay_6() { return &___delay_6; }
	inline void set_delay_6(float value)
	{
		___delay_6 = value;
	}

	inline static int32_t get_offset_of_timer_7() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___timer_7)); }
	inline float get_timer_7() const { return ___timer_7; }
	inline float* get_address_of_timer_7() { return &___timer_7; }
	inline void set_timer_7(float value)
	{
		___timer_7 = value;
	}

	inline static int32_t get_offset_of_finalized_8() { return static_cast<int32_t>(offsetof(MenuPanelController_t3752352500, ___finalized_8)); }
	inline bool get_finalized_8() const { return ___finalized_8; }
	inline bool* get_address_of_finalized_8() { return &___finalized_8; }
	inline void set_finalized_8(bool value)
	{
		___finalized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUPANELCONTROLLER_T3752352500_H
#ifndef MENUCOLORCHOICECONTROLLER_T2734251472_H
#define MENUCOLORCHOICECONTROLLER_T2734251472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuColorChoiceController
struct  MenuColorChoiceController_t2734251472  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MenuColorChoiceController::themeIndex
	int32_t ___themeIndex_2;

public:
	inline static int32_t get_offset_of_themeIndex_2() { return static_cast<int32_t>(offsetof(MenuColorChoiceController_t2734251472, ___themeIndex_2)); }
	inline int32_t get_themeIndex_2() const { return ___themeIndex_2; }
	inline int32_t* get_address_of_themeIndex_2() { return &___themeIndex_2; }
	inline void set_themeIndex_2(int32_t value)
	{
		___themeIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUCOLORCHOICECONTROLLER_T2734251472_H
#ifndef MAINMENUSCRIPT_T2493777029_H
#define MAINMENUSCRIPT_T2493777029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuScript
struct  MainMenuScript_t2493777029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MainMenuScript::colorSelectionContainer
	GameObject_t1113636619 * ___colorSelectionContainer_2;
	// UnityEngine.GameObject MainMenuScript::colorSelectionPrefab
	GameObject_t1113636619 * ___colorSelectionPrefab_3;
	// System.Single MainMenuScript::padding
	float ___padding_4;
	// System.Int32 MainMenuScript::themeIndex
	int32_t ___themeIndex_5;

public:
	inline static int32_t get_offset_of_colorSelectionContainer_2() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___colorSelectionContainer_2)); }
	inline GameObject_t1113636619 * get_colorSelectionContainer_2() const { return ___colorSelectionContainer_2; }
	inline GameObject_t1113636619 ** get_address_of_colorSelectionContainer_2() { return &___colorSelectionContainer_2; }
	inline void set_colorSelectionContainer_2(GameObject_t1113636619 * value)
	{
		___colorSelectionContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___colorSelectionContainer_2), value);
	}

	inline static int32_t get_offset_of_colorSelectionPrefab_3() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___colorSelectionPrefab_3)); }
	inline GameObject_t1113636619 * get_colorSelectionPrefab_3() const { return ___colorSelectionPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_colorSelectionPrefab_3() { return &___colorSelectionPrefab_3; }
	inline void set_colorSelectionPrefab_3(GameObject_t1113636619 * value)
	{
		___colorSelectionPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorSelectionPrefab_3), value);
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___padding_4)); }
	inline float get_padding_4() const { return ___padding_4; }
	inline float* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(float value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_themeIndex_5() { return static_cast<int32_t>(offsetof(MainMenuScript_t2493777029, ___themeIndex_5)); }
	inline int32_t get_themeIndex_5() const { return ___themeIndex_5; }
	inline int32_t* get_address_of_themeIndex_5() { return &___themeIndex_5; }
	inline void set_themeIndex_5(int32_t value)
	{
		___themeIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUSCRIPT_T2493777029_H
#ifndef DUELCONTROLLER_T4245367713_H
#define DUELCONTROLLER_T4245367713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DuelController
struct  DuelController_t4245367713  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DuelController::gameOverContainer
	GameObject_t1113636619 * ___gameOverContainer_2;
	// UnityEngine.GameObject DuelController::divider
	GameObject_t1113636619 * ___divider_3;
	// UnityEngine.AudioSource DuelController::bgMusic
	AudioSource_t3935305588 * ___bgMusic_4;
	// UnityEngine.AudioSource DuelController::failSound
	AudioSource_t3935305588 * ___failSound_5;
	// UnityEngine.AudioSource[] DuelController::safeSounds
	AudioSourceU5BU5D_t4028559421* ___safeSounds_6;
	// UnityEngine.GameObject DuelController::panelContainer
	GameObject_t1113636619 * ___panelContainer_7;
	// UnityEngine.Sprite DuelController::panelSprite
	Sprite_t280657092 * ___panelSprite_8;
	// PanelController DuelController::currentPanel
	PanelController_t1280165360 * ___currentPanel_9;
	// System.Int32 DuelController::colorScheme
	int32_t ___colorScheme_10;
	// System.Int32 DuelController::dir
	int32_t ___dir_11;
	// System.Boolean DuelController::playing
	bool ___playing_12;
	// System.Single[] DuelController::bounds
	SingleU5BU5D_t1444911251* ___bounds_13;
	// System.Single DuelController::halfwayPoint
	float ___halfwayPoint_14;

public:
	inline static int32_t get_offset_of_gameOverContainer_2() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___gameOverContainer_2)); }
	inline GameObject_t1113636619 * get_gameOverContainer_2() const { return ___gameOverContainer_2; }
	inline GameObject_t1113636619 ** get_address_of_gameOverContainer_2() { return &___gameOverContainer_2; }
	inline void set_gameOverContainer_2(GameObject_t1113636619 * value)
	{
		___gameOverContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverContainer_2), value);
	}

	inline static int32_t get_offset_of_divider_3() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___divider_3)); }
	inline GameObject_t1113636619 * get_divider_3() const { return ___divider_3; }
	inline GameObject_t1113636619 ** get_address_of_divider_3() { return &___divider_3; }
	inline void set_divider_3(GameObject_t1113636619 * value)
	{
		___divider_3 = value;
		Il2CppCodeGenWriteBarrier((&___divider_3), value);
	}

	inline static int32_t get_offset_of_bgMusic_4() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___bgMusic_4)); }
	inline AudioSource_t3935305588 * get_bgMusic_4() const { return ___bgMusic_4; }
	inline AudioSource_t3935305588 ** get_address_of_bgMusic_4() { return &___bgMusic_4; }
	inline void set_bgMusic_4(AudioSource_t3935305588 * value)
	{
		___bgMusic_4 = value;
		Il2CppCodeGenWriteBarrier((&___bgMusic_4), value);
	}

	inline static int32_t get_offset_of_failSound_5() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___failSound_5)); }
	inline AudioSource_t3935305588 * get_failSound_5() const { return ___failSound_5; }
	inline AudioSource_t3935305588 ** get_address_of_failSound_5() { return &___failSound_5; }
	inline void set_failSound_5(AudioSource_t3935305588 * value)
	{
		___failSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___failSound_5), value);
	}

	inline static int32_t get_offset_of_safeSounds_6() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___safeSounds_6)); }
	inline AudioSourceU5BU5D_t4028559421* get_safeSounds_6() const { return ___safeSounds_6; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_safeSounds_6() { return &___safeSounds_6; }
	inline void set_safeSounds_6(AudioSourceU5BU5D_t4028559421* value)
	{
		___safeSounds_6 = value;
		Il2CppCodeGenWriteBarrier((&___safeSounds_6), value);
	}

	inline static int32_t get_offset_of_panelContainer_7() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___panelContainer_7)); }
	inline GameObject_t1113636619 * get_panelContainer_7() const { return ___panelContainer_7; }
	inline GameObject_t1113636619 ** get_address_of_panelContainer_7() { return &___panelContainer_7; }
	inline void set_panelContainer_7(GameObject_t1113636619 * value)
	{
		___panelContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_7), value);
	}

	inline static int32_t get_offset_of_panelSprite_8() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___panelSprite_8)); }
	inline Sprite_t280657092 * get_panelSprite_8() const { return ___panelSprite_8; }
	inline Sprite_t280657092 ** get_address_of_panelSprite_8() { return &___panelSprite_8; }
	inline void set_panelSprite_8(Sprite_t280657092 * value)
	{
		___panelSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprite_8), value);
	}

	inline static int32_t get_offset_of_currentPanel_9() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___currentPanel_9)); }
	inline PanelController_t1280165360 * get_currentPanel_9() const { return ___currentPanel_9; }
	inline PanelController_t1280165360 ** get_address_of_currentPanel_9() { return &___currentPanel_9; }
	inline void set_currentPanel_9(PanelController_t1280165360 * value)
	{
		___currentPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanel_9), value);
	}

	inline static int32_t get_offset_of_colorScheme_10() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___colorScheme_10)); }
	inline int32_t get_colorScheme_10() const { return ___colorScheme_10; }
	inline int32_t* get_address_of_colorScheme_10() { return &___colorScheme_10; }
	inline void set_colorScheme_10(int32_t value)
	{
		___colorScheme_10 = value;
	}

	inline static int32_t get_offset_of_dir_11() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___dir_11)); }
	inline int32_t get_dir_11() const { return ___dir_11; }
	inline int32_t* get_address_of_dir_11() { return &___dir_11; }
	inline void set_dir_11(int32_t value)
	{
		___dir_11 = value;
	}

	inline static int32_t get_offset_of_playing_12() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___playing_12)); }
	inline bool get_playing_12() const { return ___playing_12; }
	inline bool* get_address_of_playing_12() { return &___playing_12; }
	inline void set_playing_12(bool value)
	{
		___playing_12 = value;
	}

	inline static int32_t get_offset_of_bounds_13() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___bounds_13)); }
	inline SingleU5BU5D_t1444911251* get_bounds_13() const { return ___bounds_13; }
	inline SingleU5BU5D_t1444911251** get_address_of_bounds_13() { return &___bounds_13; }
	inline void set_bounds_13(SingleU5BU5D_t1444911251* value)
	{
		___bounds_13 = value;
		Il2CppCodeGenWriteBarrier((&___bounds_13), value);
	}

	inline static int32_t get_offset_of_halfwayPoint_14() { return static_cast<int32_t>(offsetof(DuelController_t4245367713, ___halfwayPoint_14)); }
	inline float get_halfwayPoint_14() const { return ___halfwayPoint_14; }
	inline float* get_address_of_halfwayPoint_14() { return &___halfwayPoint_14; }
	inline void set_halfwayPoint_14(float value)
	{
		___halfwayPoint_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUELCONTROLLER_T4245367713_H
#ifndef FADEOUTCONTROLLER_T1417150664_H
#define FADEOUTCONTROLLER_T1417150664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeOutController
struct  FadeOutController_t1417150664  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean FadeOutController::fading
	bool ___fading_2;
	// System.Single FadeOutController::lerpTimer
	float ___lerpTimer_3;
	// System.Single FadeOutController::fadeSpeed
	float ___fadeSpeed_4;
	// UnityEngine.UI.Image FadeOutController::i
	Image_t2670269651 * ___i_5;

public:
	inline static int32_t get_offset_of_fading_2() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___fading_2)); }
	inline bool get_fading_2() const { return ___fading_2; }
	inline bool* get_address_of_fading_2() { return &___fading_2; }
	inline void set_fading_2(bool value)
	{
		___fading_2 = value;
	}

	inline static int32_t get_offset_of_lerpTimer_3() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___lerpTimer_3)); }
	inline float get_lerpTimer_3() const { return ___lerpTimer_3; }
	inline float* get_address_of_lerpTimer_3() { return &___lerpTimer_3; }
	inline void set_lerpTimer_3(float value)
	{
		___lerpTimer_3 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_i_5() { return static_cast<int32_t>(offsetof(FadeOutController_t1417150664, ___i_5)); }
	inline Image_t2670269651 * get_i_5() const { return ___i_5; }
	inline Image_t2670269651 ** get_address_of_i_5() { return &___i_5; }
	inline void set_i_5(Image_t2670269651 * value)
	{
		___i_5 = value;
		Il2CppCodeGenWriteBarrier((&___i_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEOUTCONTROLLER_T1417150664_H
#ifndef SQUARECONTROLLER_T2553685805_H
#define SQUARECONTROLLER_T2553685805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SquareController
struct  SquareController_t2553685805  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SquareController::highScore
	int32_t ___highScore_2;
	// System.Boolean SquareController::setHighScore
	bool ___setHighScore_3;
	// UnityEngine.UI.Text SquareController::scoreText
	Text_t1901882714 * ___scoreText_4;
	// UnityEngine.GameObject SquareController::gameOverContainer
	GameObject_t1113636619 * ___gameOverContainer_5;
	// UnityEngine.UI.Text SquareController::gameOverScoreText
	Text_t1901882714 * ___gameOverScoreText_6;
	// UnityEngine.UI.Text SquareController::gameOverRecordText
	Text_t1901882714 * ___gameOverRecordText_7;
	// UnityEngine.UI.Text SquareController::newThemeText
	Text_t1901882714 * ___newThemeText_8;
	// UnityEngine.GameObject SquareController::exportButton
	GameObject_t1113636619 * ___exportButton_9;
	// UnityEngine.AudioSource SquareController::bgMusic
	AudioSource_t3935305588 * ___bgMusic_10;
	// UnityEngine.AudioSource SquareController::drums
	AudioSource_t3935305588 * ___drums_11;
	// UnityEngine.AudioSource SquareController::applause
	AudioSource_t3935305588 * ___applause_12;
	// UnityEngine.AudioSource[] SquareController::accentSounds
	AudioSourceU5BU5D_t4028559421* ___accentSounds_13;
	// UnityEngine.AudioSource SquareController::failSound
	AudioSource_t3935305588 * ___failSound_14;
	// UnityEngine.AudioSource[] SquareController::safeSounds
	AudioSourceU5BU5D_t4028559421* ___safeSounds_15;
	// UnityEngine.GameObject SquareController::panelContainer
	GameObject_t1113636619 * ___panelContainer_16;
	// UnityEngine.GameObject SquareController::allPanelsContainer
	GameObject_t1113636619 * ___allPanelsContainer_17;
	// UnityEngine.Sprite SquareController::panelSprite
	Sprite_t280657092 * ___panelSprite_18;
	// PanelController SquareController::currentPanel
	PanelController_t1280165360 * ___currentPanel_19;
	// System.Int32 SquareController::colorScheme
	int32_t ___colorScheme_20;
	// System.Int32 SquareController::dir
	int32_t ___dir_21;
	// System.Int32 SquareController::score
	int32_t ___score_22;
	// System.Boolean SquareController::playing
	bool ___playing_23;
	// System.Single[] SquareController::bounds
	SingleU5BU5D_t1444911251* ___bounds_24;
	// System.Int32 SquareController::numThemesUnlocked
	int32_t ___numThemesUnlocked_25;
	// System.Boolean SquareController::unlockedATheme
	bool ___unlockedATheme_26;
	// FadeOutController SquareController::gameOverFade
	FadeOutController_t1417150664 * ___gameOverFade_27;

public:
	inline static int32_t get_offset_of_highScore_2() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___highScore_2)); }
	inline int32_t get_highScore_2() const { return ___highScore_2; }
	inline int32_t* get_address_of_highScore_2() { return &___highScore_2; }
	inline void set_highScore_2(int32_t value)
	{
		___highScore_2 = value;
	}

	inline static int32_t get_offset_of_setHighScore_3() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___setHighScore_3)); }
	inline bool get_setHighScore_3() const { return ___setHighScore_3; }
	inline bool* get_address_of_setHighScore_3() { return &___setHighScore_3; }
	inline void set_setHighScore_3(bool value)
	{
		___setHighScore_3 = value;
	}

	inline static int32_t get_offset_of_scoreText_4() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___scoreText_4)); }
	inline Text_t1901882714 * get_scoreText_4() const { return ___scoreText_4; }
	inline Text_t1901882714 ** get_address_of_scoreText_4() { return &___scoreText_4; }
	inline void set_scoreText_4(Text_t1901882714 * value)
	{
		___scoreText_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_4), value);
	}

	inline static int32_t get_offset_of_gameOverContainer_5() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverContainer_5)); }
	inline GameObject_t1113636619 * get_gameOverContainer_5() const { return ___gameOverContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_gameOverContainer_5() { return &___gameOverContainer_5; }
	inline void set_gameOverContainer_5(GameObject_t1113636619 * value)
	{
		___gameOverContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverContainer_5), value);
	}

	inline static int32_t get_offset_of_gameOverScoreText_6() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverScoreText_6)); }
	inline Text_t1901882714 * get_gameOverScoreText_6() const { return ___gameOverScoreText_6; }
	inline Text_t1901882714 ** get_address_of_gameOverScoreText_6() { return &___gameOverScoreText_6; }
	inline void set_gameOverScoreText_6(Text_t1901882714 * value)
	{
		___gameOverScoreText_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScoreText_6), value);
	}

	inline static int32_t get_offset_of_gameOverRecordText_7() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverRecordText_7)); }
	inline Text_t1901882714 * get_gameOverRecordText_7() const { return ___gameOverRecordText_7; }
	inline Text_t1901882714 ** get_address_of_gameOverRecordText_7() { return &___gameOverRecordText_7; }
	inline void set_gameOverRecordText_7(Text_t1901882714 * value)
	{
		___gameOverRecordText_7 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverRecordText_7), value);
	}

	inline static int32_t get_offset_of_newThemeText_8() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___newThemeText_8)); }
	inline Text_t1901882714 * get_newThemeText_8() const { return ___newThemeText_8; }
	inline Text_t1901882714 ** get_address_of_newThemeText_8() { return &___newThemeText_8; }
	inline void set_newThemeText_8(Text_t1901882714 * value)
	{
		___newThemeText_8 = value;
		Il2CppCodeGenWriteBarrier((&___newThemeText_8), value);
	}

	inline static int32_t get_offset_of_exportButton_9() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___exportButton_9)); }
	inline GameObject_t1113636619 * get_exportButton_9() const { return ___exportButton_9; }
	inline GameObject_t1113636619 ** get_address_of_exportButton_9() { return &___exportButton_9; }
	inline void set_exportButton_9(GameObject_t1113636619 * value)
	{
		___exportButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___exportButton_9), value);
	}

	inline static int32_t get_offset_of_bgMusic_10() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___bgMusic_10)); }
	inline AudioSource_t3935305588 * get_bgMusic_10() const { return ___bgMusic_10; }
	inline AudioSource_t3935305588 ** get_address_of_bgMusic_10() { return &___bgMusic_10; }
	inline void set_bgMusic_10(AudioSource_t3935305588 * value)
	{
		___bgMusic_10 = value;
		Il2CppCodeGenWriteBarrier((&___bgMusic_10), value);
	}

	inline static int32_t get_offset_of_drums_11() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___drums_11)); }
	inline AudioSource_t3935305588 * get_drums_11() const { return ___drums_11; }
	inline AudioSource_t3935305588 ** get_address_of_drums_11() { return &___drums_11; }
	inline void set_drums_11(AudioSource_t3935305588 * value)
	{
		___drums_11 = value;
		Il2CppCodeGenWriteBarrier((&___drums_11), value);
	}

	inline static int32_t get_offset_of_applause_12() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___applause_12)); }
	inline AudioSource_t3935305588 * get_applause_12() const { return ___applause_12; }
	inline AudioSource_t3935305588 ** get_address_of_applause_12() { return &___applause_12; }
	inline void set_applause_12(AudioSource_t3935305588 * value)
	{
		___applause_12 = value;
		Il2CppCodeGenWriteBarrier((&___applause_12), value);
	}

	inline static int32_t get_offset_of_accentSounds_13() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___accentSounds_13)); }
	inline AudioSourceU5BU5D_t4028559421* get_accentSounds_13() const { return ___accentSounds_13; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_accentSounds_13() { return &___accentSounds_13; }
	inline void set_accentSounds_13(AudioSourceU5BU5D_t4028559421* value)
	{
		___accentSounds_13 = value;
		Il2CppCodeGenWriteBarrier((&___accentSounds_13), value);
	}

	inline static int32_t get_offset_of_failSound_14() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___failSound_14)); }
	inline AudioSource_t3935305588 * get_failSound_14() const { return ___failSound_14; }
	inline AudioSource_t3935305588 ** get_address_of_failSound_14() { return &___failSound_14; }
	inline void set_failSound_14(AudioSource_t3935305588 * value)
	{
		___failSound_14 = value;
		Il2CppCodeGenWriteBarrier((&___failSound_14), value);
	}

	inline static int32_t get_offset_of_safeSounds_15() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___safeSounds_15)); }
	inline AudioSourceU5BU5D_t4028559421* get_safeSounds_15() const { return ___safeSounds_15; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_safeSounds_15() { return &___safeSounds_15; }
	inline void set_safeSounds_15(AudioSourceU5BU5D_t4028559421* value)
	{
		___safeSounds_15 = value;
		Il2CppCodeGenWriteBarrier((&___safeSounds_15), value);
	}

	inline static int32_t get_offset_of_panelContainer_16() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___panelContainer_16)); }
	inline GameObject_t1113636619 * get_panelContainer_16() const { return ___panelContainer_16; }
	inline GameObject_t1113636619 ** get_address_of_panelContainer_16() { return &___panelContainer_16; }
	inline void set_panelContainer_16(GameObject_t1113636619 * value)
	{
		___panelContainer_16 = value;
		Il2CppCodeGenWriteBarrier((&___panelContainer_16), value);
	}

	inline static int32_t get_offset_of_allPanelsContainer_17() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___allPanelsContainer_17)); }
	inline GameObject_t1113636619 * get_allPanelsContainer_17() const { return ___allPanelsContainer_17; }
	inline GameObject_t1113636619 ** get_address_of_allPanelsContainer_17() { return &___allPanelsContainer_17; }
	inline void set_allPanelsContainer_17(GameObject_t1113636619 * value)
	{
		___allPanelsContainer_17 = value;
		Il2CppCodeGenWriteBarrier((&___allPanelsContainer_17), value);
	}

	inline static int32_t get_offset_of_panelSprite_18() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___panelSprite_18)); }
	inline Sprite_t280657092 * get_panelSprite_18() const { return ___panelSprite_18; }
	inline Sprite_t280657092 ** get_address_of_panelSprite_18() { return &___panelSprite_18; }
	inline void set_panelSprite_18(Sprite_t280657092 * value)
	{
		___panelSprite_18 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprite_18), value);
	}

	inline static int32_t get_offset_of_currentPanel_19() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___currentPanel_19)); }
	inline PanelController_t1280165360 * get_currentPanel_19() const { return ___currentPanel_19; }
	inline PanelController_t1280165360 ** get_address_of_currentPanel_19() { return &___currentPanel_19; }
	inline void set_currentPanel_19(PanelController_t1280165360 * value)
	{
		___currentPanel_19 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanel_19), value);
	}

	inline static int32_t get_offset_of_colorScheme_20() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___colorScheme_20)); }
	inline int32_t get_colorScheme_20() const { return ___colorScheme_20; }
	inline int32_t* get_address_of_colorScheme_20() { return &___colorScheme_20; }
	inline void set_colorScheme_20(int32_t value)
	{
		___colorScheme_20 = value;
	}

	inline static int32_t get_offset_of_dir_21() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___dir_21)); }
	inline int32_t get_dir_21() const { return ___dir_21; }
	inline int32_t* get_address_of_dir_21() { return &___dir_21; }
	inline void set_dir_21(int32_t value)
	{
		___dir_21 = value;
	}

	inline static int32_t get_offset_of_score_22() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___score_22)); }
	inline int32_t get_score_22() const { return ___score_22; }
	inline int32_t* get_address_of_score_22() { return &___score_22; }
	inline void set_score_22(int32_t value)
	{
		___score_22 = value;
	}

	inline static int32_t get_offset_of_playing_23() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___playing_23)); }
	inline bool get_playing_23() const { return ___playing_23; }
	inline bool* get_address_of_playing_23() { return &___playing_23; }
	inline void set_playing_23(bool value)
	{
		___playing_23 = value;
	}

	inline static int32_t get_offset_of_bounds_24() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___bounds_24)); }
	inline SingleU5BU5D_t1444911251* get_bounds_24() const { return ___bounds_24; }
	inline SingleU5BU5D_t1444911251** get_address_of_bounds_24() { return &___bounds_24; }
	inline void set_bounds_24(SingleU5BU5D_t1444911251* value)
	{
		___bounds_24 = value;
		Il2CppCodeGenWriteBarrier((&___bounds_24), value);
	}

	inline static int32_t get_offset_of_numThemesUnlocked_25() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___numThemesUnlocked_25)); }
	inline int32_t get_numThemesUnlocked_25() const { return ___numThemesUnlocked_25; }
	inline int32_t* get_address_of_numThemesUnlocked_25() { return &___numThemesUnlocked_25; }
	inline void set_numThemesUnlocked_25(int32_t value)
	{
		___numThemesUnlocked_25 = value;
	}

	inline static int32_t get_offset_of_unlockedATheme_26() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___unlockedATheme_26)); }
	inline bool get_unlockedATheme_26() const { return ___unlockedATheme_26; }
	inline bool* get_address_of_unlockedATheme_26() { return &___unlockedATheme_26; }
	inline void set_unlockedATheme_26(bool value)
	{
		___unlockedATheme_26 = value;
	}

	inline static int32_t get_offset_of_gameOverFade_27() { return static_cast<int32_t>(offsetof(SquareController_t2553685805, ___gameOverFade_27)); }
	inline FadeOutController_t1417150664 * get_gameOverFade_27() const { return ___gameOverFade_27; }
	inline FadeOutController_t1417150664 ** get_address_of_gameOverFade_27() { return &___gameOverFade_27; }
	inline void set_gameOverFade_27(FadeOutController_t1417150664 * value)
	{
		___gameOverFade_27 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverFade_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SQUARECONTROLLER_T2553685805_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (ColorSchemeManager_t1974266384), -1, sizeof(ColorSchemeManager_t1974266384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	ColorSchemeManager_t1974266384_StaticFields::get_offset_of_colorSchemes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (DuelController_t4245367713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[13] = 
{
	DuelController_t4245367713::get_offset_of_gameOverContainer_2(),
	DuelController_t4245367713::get_offset_of_divider_3(),
	DuelController_t4245367713::get_offset_of_bgMusic_4(),
	DuelController_t4245367713::get_offset_of_failSound_5(),
	DuelController_t4245367713::get_offset_of_safeSounds_6(),
	DuelController_t4245367713::get_offset_of_panelContainer_7(),
	DuelController_t4245367713::get_offset_of_panelSprite_8(),
	DuelController_t4245367713::get_offset_of_currentPanel_9(),
	DuelController_t4245367713::get_offset_of_colorScheme_10(),
	DuelController_t4245367713::get_offset_of_dir_11(),
	DuelController_t4245367713::get_offset_of_playing_12(),
	DuelController_t4245367713::get_offset_of_bounds_13(),
	DuelController_t4245367713::get_offset_of_halfwayPoint_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (FadeOutController_t1417150664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	FadeOutController_t1417150664::get_offset_of_fading_2(),
	FadeOutController_t1417150664::get_offset_of_lerpTimer_3(),
	FadeOutController_t1417150664::get_offset_of_fadeSpeed_4(),
	FadeOutController_t1417150664::get_offset_of_i_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (MainMenuScript_t2493777029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	MainMenuScript_t2493777029::get_offset_of_colorSelectionContainer_2(),
	MainMenuScript_t2493777029::get_offset_of_colorSelectionPrefab_3(),
	MainMenuScript_t2493777029::get_offset_of_padding_4(),
	MainMenuScript_t2493777029::get_offset_of_themeIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (MenuColorChoiceController_t2734251472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	MenuColorChoiceController_t2734251472::get_offset_of_themeIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (MenuPanelController_t3752352500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[7] = 
{
	MenuPanelController_t3752352500::get_offset_of_moving_2(),
	MenuPanelController_t3752352500::get_offset_of_speed_3(),
	MenuPanelController_t3752352500::get_offset_of_maxHeight_4(),
	MenuPanelController_t3752352500::get_offset_of_rectTransform_5(),
	MenuPanelController_t3752352500::get_offset_of_delay_6(),
	MenuPanelController_t3752352500::get_offset_of_timer_7(),
	MenuPanelController_t3752352500::get_offset_of_finalized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (PanelContainerController_t2722594607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[8] = 
{
	PanelContainerController_t2722594607::get_offset_of_shrinking_2(),
	PanelContainerController_t2722594607::get_offset_of_startScale_3(),
	PanelContainerController_t2722594607::get_offset_of_endScale_4(),
	PanelContainerController_t2722594607::get_offset_of_lerpTimer_5(),
	PanelContainerController_t2722594607::get_offset_of_shrinkSpeed_6(),
	PanelContainerController_t2722594607::get_offset_of_screenshotCountdown_7(),
	PanelContainerController_t2722594607::get_offset_of_takingScreenshot_8(),
	PanelContainerController_t2722594607::get_offset_of_hiddenForScreenshot_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (PanelController_t1280165360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[10] = 
{
	PanelController_t1280165360::get_offset_of_moving_2(),
	PanelController_t1280165360::get_offset_of_speed_3(),
	PanelController_t1280165360::get_offset_of_dir_4(),
	PanelController_t1280165360::get_offset_of_deathPoint_5(),
	PanelController_t1280165360::get_offset_of_square_6(),
	PanelController_t1280165360::get_offset_of_duel_7(),
	PanelController_t1280165360::get_offset_of_rectTransform_8(),
	PanelController_t1280165360::get_offset_of_adjustedSpeed_9(),
	PanelController_t1280165360::get_offset_of_leadingEdgeSprite_10(),
	PanelController_t1280165360::get_offset_of_leadingEdge_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (SquareController_t2553685805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[26] = 
{
	SquareController_t2553685805::get_offset_of_highScore_2(),
	SquareController_t2553685805::get_offset_of_setHighScore_3(),
	SquareController_t2553685805::get_offset_of_scoreText_4(),
	SquareController_t2553685805::get_offset_of_gameOverContainer_5(),
	SquareController_t2553685805::get_offset_of_gameOverScoreText_6(),
	SquareController_t2553685805::get_offset_of_gameOverRecordText_7(),
	SquareController_t2553685805::get_offset_of_newThemeText_8(),
	SquareController_t2553685805::get_offset_of_exportButton_9(),
	SquareController_t2553685805::get_offset_of_bgMusic_10(),
	SquareController_t2553685805::get_offset_of_drums_11(),
	SquareController_t2553685805::get_offset_of_applause_12(),
	SquareController_t2553685805::get_offset_of_accentSounds_13(),
	SquareController_t2553685805::get_offset_of_failSound_14(),
	SquareController_t2553685805::get_offset_of_safeSounds_15(),
	SquareController_t2553685805::get_offset_of_panelContainer_16(),
	SquareController_t2553685805::get_offset_of_allPanelsContainer_17(),
	SquareController_t2553685805::get_offset_of_panelSprite_18(),
	SquareController_t2553685805::get_offset_of_currentPanel_19(),
	SquareController_t2553685805::get_offset_of_colorScheme_20(),
	SquareController_t2553685805::get_offset_of_dir_21(),
	SquareController_t2553685805::get_offset_of_score_22(),
	SquareController_t2553685805::get_offset_of_playing_23(),
	SquareController_t2553685805::get_offset_of_bounds_24(),
	SquareController_t2553685805::get_offset_of_numThemesUnlocked_25(),
	SquareController_t2553685805::get_offset_of_unlockedATheme_26(),
	SquareController_t2553685805::get_offset_of_gameOverFade_27(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
